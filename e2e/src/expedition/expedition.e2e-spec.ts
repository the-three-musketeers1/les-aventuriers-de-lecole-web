import {AppPage} from "../app.po";
import {browser, by, element, logging} from "protractor";
import {ExpeditionPage} from "./expedition.po";

describe('workspace-project App', () => {
  let page: ExpeditionPage;

  const credentias = {
    hash: 'le_token_de_mama',
    characterId: 2,
    uid: 5
  };

  beforeAll(() => {
    page = new ExpeditionPage();
    page.navigateTo();
    browser.executeScript("window.localStorage.clear();");
    browser.executeScript("window.localStorage.setItem('characterId', '"+ credentias.characterId +"');");
    browser.executeScript("window.localStorage.setItem('hash', '"+ credentias.hash +"');");
    browser.executeScript("window.localStorage.setItem('uid', '"+ credentias.uid +"');");
  });

  it('should display connexion home page', () => {
    page.navigateToCurrent();
    expect(element(by.tagName('h2')).getText()).toEqual('Expéditions disponibles :');
  });

  it('should display expedition historic page', () => {
    page.navigateToHistoric();
    expect(page.getTitleText()).toEqual('Liste des expéditions :');
  });

  it('should get expedition in table', () => {
    page.navigateToCurrent();
    expect(element.all(by.css("table tbody tr")).count()).toEqual(1);
  });
  it('should display view of exercises expedition', () => {
    page.navigateToCurrent();
    page.navigateToElementOfList(0);
    expect(page.getTitleText()).toEqual('Les puissances');
  });

  it('should get at least 2 exercises in list', () => {
    page.navigateToCurrent();
    page.navigateToElementOfList(0);
    expect(element.all(by.css("table tbody tr")).count()).toEqual(2);
  });

  it('should start exercise', () => {
    page.navigateToCurrent();
    page.navigateToElementOfList(0);
    page.navigateToElementOfList(0);
    browser.waitForAngularEnabled(false);
    browser.sleep(3*1000);
    expect(element(by.css('#question')).getText()).toEqual("Racine carré de 9");
    expect(element.all(by.css(".answer")).count()).toEqual(2);
    browser.waitForAngularEnabled(true);
  });

  it('should do exercise', () => {
    page.navigateToCurrent();
    page.navigateToElementOfList(0);
    page.navigateToElementOfList(0);
    browser.waitForAngularEnabled(false);
    browser.sleep(2*1000);
    page.selectAnswer(0);
    browser.sleep(3*1000);
    page.validateAnswer();
    browser.sleep(5.5*1000);
    page.insertAnswerForInputQuestion("2,4");
    page.validateAnswer();
    expect(page.getExerciseEndIndication()).toEqual("Fin de l'exercice");
    expect(page.getExerciseMark()).toEqual("NOTE : 20/20");
    expect(page.getMonsterState()).toEqual("Vous avez vaincu le monstre !");
    browser.waitForAngularEnabled(true);
  });

  it('should not start exercise when he is already done', () => {
    page.navigateToCurrent();
    page.navigateToElementOfList(0);
    page.navigateToElementOfList(0);
    page.navigateToElementOfList(0);
    var exercise = page.getRowOfList(0);
    expect(exercise.get(8).getText()).toEqual("20 / 20");
    expect(page.getTitleText()).toEqual('Les puissances');
  });

  it('should show message when end all expedition', () => {
    page.navigateToCurrent();
    page.navigateToElementOfList(0);
    page.navigateToElementOfList(1);
    browser.waitForAngularEnabled(false);
    browser.sleep(2*1000);
    page.selectAnswer(0);
    browser.sleep(3*1000);
    page.validateAnswer();
    browser.sleep(5.5*1000);
    page.insertAnswerForInputQuestion("2,4");
    page.validateAnswer();
    expect(page.getExerciseEndIndication()).toEqual("Fin de l'exercice");
    browser.sleep(2*1000);
    expect(page.getToastMessage()).toEqual(true);
    browser.waitForAngularEnabled(true);
  });

  it('Expedition row should have thumbs-up', () => {
    page.navigateToCurrent();
    let row = page.getRowOfList(0);
    let result = row.get(6);
    expect(result.all(by.tagName("button")).get(0).getAttribute("icon")).toEqual("pi pi-thumbs-up");
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));

  });
});
