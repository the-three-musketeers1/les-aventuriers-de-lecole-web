import {browser, by, element} from "protractor";

export class ExpeditionPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }
  navigateToHistoric(): Promise<unknown> {
    return browser.get(browser.baseUrl + "expeditions") as Promise<unknown>;
  }
  navigateToCurrent(): Promise<unknown> {
    return browser.get(browser.baseUrl + "resume") as Promise<unknown>;
  }
  navigateToElementOfList(number) {
    element.all(by.css("table tbody tr")).get(number).click();
  }
  getRowOfList(number) {
    return element.all(by.css("table tbody tr")).get(number).all(by.css("td"));
  }

  getTitleText(): Promise<string> {
    return element(by.tagName('h2')).getText() as Promise<string>;
  }

  selectAnswer(number: number) {
    element.all(by.css(".answer")).get(number).click();
  }
  insertAnswerForInputQuestion(str: string) {
    element(by.css("#input-user")).sendKeys(str);
  }
  validateAnswer() {
    element(by.css("#btn-validate")).click();
  }
  getExerciseEndIndication() {
    return element(by.css("#exercise-end")).getText();
  }
  getExerciseMark() {
    return element(by.css("#exercise-mark")).getText();
  }
  getMonsterState() {
    return element(by.css("#monster-state")).getText();
  }

  getToastMessage() {
    return element(by.css('#end-expedition')).isPresent();
  }
}
