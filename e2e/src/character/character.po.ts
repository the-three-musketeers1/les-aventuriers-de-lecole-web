import {browser, by, element, ElementArrayFinder, ElementFinder} from 'protractor';

export class CharacterPage {
  navigateTo(url= browser.baseUrl): Promise<unknown> {
    return browser.get(url) as Promise<unknown>;
  }

  async setTokenAndNavigateTo(credentials: { uid: string; password: string; hash: string; username: string }) {
    await this.navigateTo();
    await browser.executeScript('window.localStorage.setItem(\'hash\', \'' + credentials.hash + '\');');
    await browser.executeScript('window.localStorage.setItem(\'uid\', \'' + credentials.uid + '\');');
    await this.navigateTo(browser.baseUrl + 'characters');
  }

  async navigateToCharacters() {
    await this.navigateTo(browser.baseUrl + 'characters');
  }


  getAllTrRowsTableCharacters(): ElementArrayFinder {
    return element(by.css('table > tbody')).all(by.tagName('tr'));
  }

  getButtonToAddCharacter(): ElementFinder {
    return element(by.css('#add-character'));
  }

  async navigateToCharacterForm(guildName: string, guildToken: string): Promise<void> {
    const button = this.getButtonToAddCharacter();
    await button.click();
    await element(by.css('#guild-name')).sendKeys(guildName);
    await element(by.css('#guild-token')).sendKeys(guildToken);
    await element(by.css('#check-guild-button')).click();
  }
}
