import {browser, by, element} from 'protractor';
import {CharacterPage} from './character.po';

describe('workspace-project Character', () => {
  let page: CharacterPage;

  const credentials = {
    username: 'mama@gmail.com',
    password: 'mama',
    hash: 'le_token_de_mama',
    uid: '5',
    guildName: 'guildDeadlineAvailable',
    guildToken: 'tokenGuildDeadlineAvailable',
    newCharacterName: 'Nouveau Guerrier',
    alreadyGuildName: 'guild2',
    alreadyGuildToken: 'tokenGuild2'
  };

  beforeAll(async () => {
    page = new CharacterPage();
    await page.setTokenAndNavigateTo(credentials);
  });

  describe('in default page', () => {
    it('should have button to create button', () => {
      const button = page.getButtonToAddCharacter();

      expect(button).toBeDefined();
      expect(button.getTagName()).toEqual('button');
      expect(button.getText()).toEqual('Créer votre personnage');
    });
  });

  describe('in success scenario', () => {
    beforeEach(async () => {
      await page.navigateTo();
    });
    it('should access to check guild component after click to create character button', async () => {
      const button = page.getButtonToAddCharacter();
      await button.click();

      expect(element(by.css('#checking-guild-component')).getTagName()).toEqual('div');
    });
    it('should verify guild is available and go to character form', async () => {
      const button = page.getButtonToAddCharacter();
      await button.click();
      await element(by.css('#guild-name')).sendKeys(credentials.guildName);
      await element(by.css('#guild-token')).sendKeys(credentials.guildToken);
      await element(by.css('#check-guild-button')).click();

      expect(element(by.css('#form-character-component')).getTagName()).toEqual('div');
      expect(element(by.tagName('h2')).getText()).toEqual('Création de votre personnage');
    });
    it('should have 4 roles selectable and Guerrier per default', async () => {
      await page.navigateToCharacterForm(credentials.guildName, credentials.guildToken);
      const roles = element.all(by.tagName('option'));

      expect(roles.count()).toBe(4);
      expect(roles.get(0).getText()).toEqual('Guerrier');
    });
    it('should create character and access in character page', async () => {
      await page.navigateToCharacterForm(credentials.guildName, credentials.guildToken);
      await element(by.id('name')).sendKeys(credentials.newCharacterName);
      await element(by.id('create-button')).click();

      expect(element(by.tagName('app-resume'))).not.toBe(null);
      expect(element.all(by.css('.profil-information')).get(1).getText()).toBe(credentials.newCharacterName);
    });
    it('should add new character in list of character', async () => {
      await page.navigateToCharacters();
      const trs = page.getAllTrRowsTableCharacters();

      expect(trs.count()).toBe(2);
    });
  });

  describe('fail scenario', () => {
    it('should show error window when user try to create character in guild that has already his or her character', async () => {
      await page.navigateTo();
      await page.navigateToCharacterForm(credentials.alreadyGuildName, credentials.alreadyGuildToken);
      await element(by.id('name')).sendKeys(credentials.newCharacterName);
      expect(element(by.tagName('p-dialog')).getAttribute('ng-reflect-visible')).toBe('false');

      await element(by.id('create-button')).click();

      expect(element(by.tagName('p-dialog')).getAttribute('ng-reflect-visible')).toBe('true');
      expect(element(by.css('#form-character-component')).getTagName()).toEqual('div');
      browser.manage().logs().get('browser');
    });
  });
});
