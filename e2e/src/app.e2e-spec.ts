import { AppPage } from './app.po';
import {browser, by, element, logging} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  const credentias = {
    username: 'francois@gmail.com',
    password: 'francois'
  };

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    browser.executeScript("window.localStorage.clear();");
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Les Aventuriers de l\'Ecole');
  });

  it('should go to subscribe page',  () => {
    page.navigateTo();
    element(by.css('#subscribe-btn')).click();
    expect(element(by.tagName('h2')).getText()).toEqual('Créer votre compte')
  });

  it('should go to login page',  () => {
    page.navigateTo();
    element(by.css('#login-btn')).click();
    expect(element(by.tagName('h2')).getText()).toEqual('Connexion')
  });

  it('should login',  () => {
    page.navigateTo();
    element(by.css('#login-btn')).click();
    expect(element(by.tagName('h2')).getText()).toEqual('Connexion');
    element(by.css('#email')).sendKeys(credentias.username);
    element(by.css('#password')).sendKeys(credentias.password);
    element(by.css('#submit-btn')).click();
    expect(element(by.tagName('h2')).getText()).toEqual('Liste des personnages');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
