import {browser, by, element} from "protractor";

export class MessagePage {

    async navigateTo(): Promise<unknown> {
        return await browser.get(browser.baseUrl) as Promise<unknown>;
    }

    async navigateToTicketPage() {
        return await browser.get(browser.baseUrl + "/tickets") as Promise<unknown>;
    }

    async navigateToMessagePage() {
        await this.filterByTitleAsc();
        await element.all(by.css('.ui-selectable-row')).get(0).click();
    }

    async setLocalStorage() {
        await browser.executeScript('window.localStorage.clear();');
        await browser.executeScript("window.localStorage.setItem('characterId', '2');");
        await browser.executeScript("window.localStorage.setItem('hash', 'le_token_de_mama');");
        await browser.executeScript("window.localStorage.setItem('uid', '5');");
    }

    async filterByTitleAsc() {
        await element(by.id('filterByTitle')).click();
    }

    async resetStatusDropdown() {
        const i = element(by.css('.messageStatusesDropdown i'));
        await i.click();
    }

    async getReceivedMessages() {
        const dropdown = element(by.css('.messageStatusesDropdown'));
        await dropdown.click();
        await element.all(by.css('.messageStatusesDropdown li')).get(1).click();
    }

    async getSentMessages() {
        const dropdown = element(by.css('.messageStatusesDropdown'));
        await dropdown.click();
        await element.all(by.css('.messageStatusesDropdown li')).get(0).click();
    }

    async checkOnlyReceivedMessages() {
        const statuses = element.all(by.css('.ui-selectable-row #messageStatusSpan'));
        statuses.each(async (element, index) => {
            const txt = await element.getText();
            if(txt !== "Reçu") {
                return false;
            }
        });
        await this.resetStatusDropdown();
        return await statuses.count() > 0;
    }

    async checkOnlySentMessages() {
        const statuses = element.all(by.css('.ui-selectable-row #messageStatusSpan'));
        statuses.each(async (element, index) => {
            const txt = await element.getText();
            if(txt !== "Envoyé") {
                return false;
            }
        });
        await this.resetStatusDropdown();
        return await statuses.count() > 0;
    }


    async clickOnNewMessage() {
        await element(by.id('sendNewMsgBtn')).click();
    }

    async fillMessageForm() {
        await element(by.css("textarea[formControlName='messageContent']"))
            .sendKeys('Je suis fatigué');
    }

    async countMessages(): Promise<number> {
        const rows = element.all(by.css('.ui-selectable-row'));
        return rows.count();
    }

    async sendMessage() {
        await element(by.id('sendMsgBtn')).click();
    }

    async getLastMessageStatus() {
        await element(by.id('filterBySentDate')).click();
        await element(by.id('filterBySentDate')).click();
        const message = element.all(by.css('.ui-selectable-row #messageStatusSpan')).first();
        return message.getText();
    }

    async closeConfirmationModal() {
        await element(by.css('#sendMsgSucceeded a')).click();
    }
}
