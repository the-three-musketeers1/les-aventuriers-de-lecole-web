import {MessagePage} from "./message.po";
import {browser, by, element, logging} from 'protractor';

describe('message tests', async () => {
  let messagePage: MessagePage;
  let messageCount: number;

  beforeAll( async () => {
    messagePage = new MessagePage();
    await messagePage.navigateTo();
    await messagePage.setLocalStorage();
    await messagePage.navigateToTicketPage();
    await messagePage.navigateToMessagePage();
    messageCount = await messagePage.countMessages();
  });

  it('we should be in the messages view',  async () => {
    await expect(element(by.id('mainDivMessageView')).isPresent()).toEqual(true);
  });

  it('should be only sent message when status defined to sent',  async () => {
    await messagePage.getSentMessages();
    await expect(messagePage.checkOnlySentMessages()).toEqual(true);
  });

  it('should be only received message when status defined to received',  async () => {
    await messagePage.getReceivedMessages();
    await expect(messagePage.checkOnlyReceivedMessages()).toEqual(true);
  });

  it('should display modal to send new message',  async () => {
    await messagePage.clickOnNewMessage();
    await expect(element(by.id('dialogSendNewMsg')).isPresent()).toEqual(true);
  });

  it('should not be able to send a new message if the message is empty',  async () => {
    await expect(element(by.id('sendMsgBtn')).isEnabled()).toEqual(false);
  });

  it('should be able to send a new message if the message is complete',  async () => {
    await messagePage.fillMessageForm();
    await expect(element(by.id('sendMsgBtn')).isEnabled()).toEqual(true);
  });

  it('should show confirmation modal when sending new message',  async () => {
    await messagePage.sendMessage();
    await expect(element(by.id('sendMsgSucceeded')).isDisplayed()).toEqual(true);
    await messagePage.closeConfirmationModal();
  });

  it('should be one more message when send a new one',  async () => {
    const messageNumber = await messagePage.countMessages();
    await expect(messageNumber).toEqual(messageCount + 1);
  });

  it('new message should have status Envoyé',  async () => {
    await expect(messagePage.getLastMessageStatus()).toEqual('ENVOYÉ');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
