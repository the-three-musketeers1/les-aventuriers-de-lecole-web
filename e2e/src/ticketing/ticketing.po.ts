import {browser, by, element} from "protractor";

export class TicketingPage {

    async navigateTo(): Promise<unknown> {
        return await browser.get(browser.baseUrl) as Promise<unknown>;
    }

    async navigateToTicketPage(): Promise<unknown> {
        return await browser.get(browser.baseUrl + "/tickets") as Promise<unknown>;
    }

    async setLocalStorage() {
        await browser.executeScript('window.localStorage.clear();');
        await browser.executeScript("window.localStorage.setItem('characterId', '2');");
        await browser.executeScript("window.localStorage.setItem('hash', 'le_token_de_mama');");
        await browser.executeScript("window.localStorage.setItem('uid', '5');");
    }

    async displayModalToSendNewTicket() {
        await element(by.id('sendTicketBtn')).click();
    }

     async emptyMessageInTicketForm() {
        await element(by.css("input[formControlName='ticketTitle']"))
            .sendKeys('Expédition des racines carrés impossible ?');
        await element(by.css("textarea[formControlName='ticketMessage']"))
            .sendKeys('');
    }

     async emptyTitleInTicketForm() {
        await element(by.css("input[formControlName='ticketTitle']")).clear();
        element(by.css("textarea[formControlName='ticketMessage']"))
            .sendKeys('Mais c\'est quoi cette expédition trop dur sérieux ?');
    }

     async fillTicketForm() {
         await element(by.css("input[formControlName='ticketTitle']"))
            .sendKeys('Expédition des racines carrés impossible ?');
         await element(by.css("textarea[formControlName='ticketMessage']"))
            .sendKeys('Mais c\'est quoi cette expédition trop dur sérieux ? Racine carré de 6 en 3 secondes ????!');
    }

    async sendTicket() {
        await element(by.id('confirmTicketBtn')).click();
    }
}
