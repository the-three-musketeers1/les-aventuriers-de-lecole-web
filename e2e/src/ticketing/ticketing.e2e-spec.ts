import {TicketingPage} from "./ticketing.po";
import {browser, by, element, logging} from 'protractor';

describe('ticketing tests', async () => {
  let ticketingPage: TicketingPage;

  beforeAll( async () => {
    ticketingPage = new TicketingPage();
    await ticketingPage.navigateTo();// dirige vers /
    await ticketingPage.setLocalStorage();
    await ticketingPage.navigateToTicketPage(); // dirige vers /tickets
  });


  it('should display modal to send ticket',  async () => {
    await ticketingPage.displayModalToSendNewTicket();
    await expect(element(by.id('sendTicketDialog')).isDisplayed()).toEqual(true);
  });

  it('should not send ticket if message is empty',  async () => {
    await ticketingPage.emptyMessageInTicketForm();
    await expect(element(by.id('confirmTicketBtn')).isEnabled()).toEqual(false);
  });

  it('should not send ticket if title is empty',  async () => {
    await ticketingPage.emptyTitleInTicketForm();
    await expect(element(by.id('confirmTicketBtn')).isEnabled()).toEqual(false);
  });

  it('should send ticket if form is fuly filled',  async () => {
    await ticketingPage.fillTicketForm();
    await expect(element(by.id('confirmTicketBtn')).isEnabled()).toEqual(true);
  });

  it('should show modal that shows ticket envoyé when the ticket has been sent',  async () => {
    await ticketingPage.sendTicket();
    await expect(element(by.id('sendSucceeded')).isDisplayed()).toEqual(true);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
