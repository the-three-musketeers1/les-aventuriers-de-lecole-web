import {browser, by, element} from "protractor";

export class TicketingPage {

    async navigateTo(): Promise<unknown> {
        return await browser.get(browser.baseUrl) as Promise<unknown>;
    }

    async navigateToTicketPage(): Promise<unknown> {
        return await browser.get(browser.baseUrl + "/tickets") as Promise<unknown>;
    }

    async setLocalStorage() {
        await browser.executeScript('window.localStorage.clear();');
        await browser.executeScript("window.localStorage.setItem('characterId', '2');");
        await browser.executeScript("window.localStorage.setItem('hash', 'le_token_de_mama');");
        await browser.executeScript("window.localStorage.setItem('uid', '5');");
    }

    async filterByTitleAsc() {
        await element(by.id('filterByTitle')).click();
    }

    getFirstRowTitle() {
       return element.all(by.css('.ui-selectable-row #tdTitle')).get(0).getText();
    }

    async checkRowStatusClot() {
        const statuses = element.all(by.css('.ui-selectable-row #spanTicketStatus'));
        statuses.each(async (element, index) => {
            const txt = await element.getText();
            if(txt !== "CLÔT") {
                return false;
            }
        });
        return await statuses.count() > 0;
    }

    async filterByStatusClot() {
        const dropdown = element(by.css('.dropdownStatus'));
        await dropdown.click();
        await element.all(by.css('.dropdownStatus li')).get(3).click();
    }




}
