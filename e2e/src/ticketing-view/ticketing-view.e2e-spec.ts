import {TicketingPage} from "./ticketing-view.po";
import {browser, by, element, logging} from 'protractor';

describe('ticketing view tests', async () => {
  let ticketingPage: TicketingPage;

  beforeAll( async () => {
    ticketingPage = new TicketingPage();
    await ticketingPage.navigateTo();// dirige vers /
    await ticketingPage.setLocalStorage();
    await ticketingPage.navigateToTicketPage(); // dirige vers /tickets
  });

  it('we should be in the tickets view',  async () => {
    await expect(element(by.id('mainDivTicketView')).isPresent()).toEqual(true);
  });

  it('there should be 3 tickets in the list', async () => {
    expect(element.all(by.css('.ui-selectable-row')).count()).toBe(3);
  });

  it('should sort by title when clicking title header', async () => {
    await ticketingPage.filterByTitleAsc();
    await expect(ticketingPage.getFirstRowTitle()).toEqual("Est-ce que ça m'intéresse ?");
  });

  it('should only display tickets with specific status when select specific status', async () => {
    await ticketingPage.filterByStatusClot();
    await expect(ticketingPage.checkRowStatusClot()).toEqual(true);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
