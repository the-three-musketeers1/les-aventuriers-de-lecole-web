import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateUtilsService {

  constructor() { }

  formatDateToString(date: Date): string {
    date = new Date(date);
    return date.getFullYear() + '-' + this.toTwoDigitsDate(date.getMonth() + 1) + '-' + this.toTwoDigitsDate(date.getDate() + 1);
  }

  toTwoDigitsDate(number: number): string {
    return (number < 10 && number >= 0 ) ? '0' + number : number.toString();
  }

  getDisplayableFrenchDateWithoutTime(date: Date): string {
    date = new Date(date);
    return this.toTwoDigitsDate(date.getDate()) + '/' + this.toTwoDigitsDate(date.getMonth() + 1) + '/' + date.getFullYear();
  }
}
