export interface CharacterInfoDto {
  pseudo?: string;
  level?: number;
  experience?: number;
  action_points?: number;
  role?: string;
  guild_bonus?: string;
  guild_bonus_cost?: number;
  board_bonus?: string;
  board_bonus_cost?: number;
  first_name?: string;
  last_name?: string;
  email?:string;
  guild_name?:string;
}
