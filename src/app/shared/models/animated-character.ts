import {CharacterPosition} from "./character-position";

export class AnimatedCharacter {
  filePath?: string;
  currentPosition?: CharacterPosition;
  waitingPosition?: CharacterPosition;
  takeDmgPosition?: CharacterPosition;
  attackPosition?: CharacterPosition;
  name?: String;
}
