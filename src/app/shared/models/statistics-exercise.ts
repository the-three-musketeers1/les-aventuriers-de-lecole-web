export interface StatisticsExercise {
  characterExerciseId?: number;
  exerciseLife?: number;
  discipline?: string;
  chapter?: string;
  goodAnswers?: number;
  badAnswers?: number;
  damage?: number;
}
