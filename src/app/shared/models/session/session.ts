import {User} from '../user/user';

export interface Session {
  id?: number;
  token?: string;
  user?: User;
}
