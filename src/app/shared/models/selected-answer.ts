import {Answer} from "./answer";

export class SelectedAnswer {
  answer: Answer;
  questionId: number;
  bonusConsumed: boolean;
}
