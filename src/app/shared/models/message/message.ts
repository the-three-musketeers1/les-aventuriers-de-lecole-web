import {Ticket} from "../ticket/ticket";

export interface Message {
    id?: number;
    content?: number;
    sentByCharacter?: boolean;
    sentDate?: Date;
    ticket?: Ticket;
    state?: {label: string, value: number};
}
