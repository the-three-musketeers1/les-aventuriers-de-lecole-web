export class ExpeditionInformation {
  id: number;
  title: string;
  description: string;
  disciplines: string[];
  done: boolean;
  exercisesNumber: number;
  experienceBonus: number;
  deadline: Date;
  accessible: boolean;


  constructor(id?: number, title?: string, description?: string, discipline?: string[], done?: boolean, exerciseNumber?: number, experienceBonus?: number, deadline?: Date, accessible?: boolean) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.disciplines = discipline;
    this.done = done;
    this.exercisesNumber = exerciseNumber;
    this.experienceBonus = experienceBonus;
    this.deadline = deadline;
    this.accessible = accessible;
  }
}
