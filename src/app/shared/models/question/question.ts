export interface Question {
  id?: number;
  wording?: string;
  sequence_number?: number;
  answers?: any[];
  time?: number;
  questionType?: string;
  createAt?: string;
  updateAt?: string;
  damage?: number;

}
