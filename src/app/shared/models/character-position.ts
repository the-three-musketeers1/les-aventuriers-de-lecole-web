export interface CharacterPosition {
  x?: number;
  y?: number;
  sw?: number;
  sh?: number;
  dx?: number;
  dy?: number;
  dw?: number;
  dh?: number;
}
