import {Character} from '../character/character';
import {Ticket} from "../ticket/ticket";

export interface User {
  id?: number;
  email?: string;
  firstname?: string;
  lastname?: string;
  password?: string;
  characters?: Character[];
  sent_messages?: Ticket[];
}
