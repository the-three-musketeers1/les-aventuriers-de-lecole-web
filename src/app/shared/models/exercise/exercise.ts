import {Question} from '../question/question';

export interface Exercise {
  id?: number
  name?: string;
  description?: string;
  life?: number;
  subject?: string;
  chapter?: string;
  discipline?: string;
  questions?: Question[];
  validated?: boolean;
  experience?: number;
}
