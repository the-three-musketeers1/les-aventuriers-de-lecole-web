

export interface ExerciseWithCharacterData {
  id?: number
  name?: string;
  description?: string;
  life?: number;
  subject?: string;
  chapter?: string;
  discipline?: string;
  questionNumber?: number;
  validated?: boolean;
  damageDone?: number;
  mark?: number;
  experience?: number
}
