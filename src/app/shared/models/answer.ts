export interface Answer {
  content?: string;
  correct?: boolean;
  damage?: number;
  id?: number;
}
