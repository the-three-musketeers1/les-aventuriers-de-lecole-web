import {Role} from '../role/role';
import {User} from '../user/user';

export interface Character {
  id?: number;
  name?: string;
  level?: number;
  action_points?: number;
  experience?: number;
  role?: Role;
  user?: User;
}
