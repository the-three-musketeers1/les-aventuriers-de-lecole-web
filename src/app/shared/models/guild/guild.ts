import {User} from '../user/user';
import {Role} from '../role/role';
import {Character} from '../character/character';

export interface Guild {
  id?: number;
  name?: string;
  token?: string;
  deadlineAddCharacter?: Date;
  characters?: Character[];
  parentGuild?: Guild;
  childGuilds?: Guild[];
  leader?: User;
  role?: Role;
  createdDate?: Date;
  updatedDate?: Date;
}
