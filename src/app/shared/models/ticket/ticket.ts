import {User} from '../user/user';
import {Status} from "../status/status";
import {Character} from "../character/character";
import {Message} from "../message/message";

export interface Ticket {
  displayStatus?: { label: string, value: number };
  id?: number;
  title?:string;
  messages?: Message[];
  status?: Status;
  date?: Date;
  leader?: User;
  character?: Character;
}
