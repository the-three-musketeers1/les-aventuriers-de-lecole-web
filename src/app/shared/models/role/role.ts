import {Character} from '../character/character';

export interface Role {
  id?: number;
  name?: string;
  guild_bonus?: string;
  guild_bonus_cost?: number;
  board_bonus?: string;
  board_bonus_cost?: number;
  characters?: Character[];
}
