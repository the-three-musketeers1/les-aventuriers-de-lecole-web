export class AdventureInformation {
  name: string;
  subject: string;
  chapter: string;
  constructor(name, subject, chapter) {
    this.name = name;
    this.subject = subject;
    this.chapter = chapter;
  }

  }
