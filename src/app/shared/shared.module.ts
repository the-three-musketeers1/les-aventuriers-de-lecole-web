import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CheckboxModule} from 'primeng/checkbox';
import {FormsModule} from '@angular/forms';
import {ButtonModule} from 'primeng/button';
import {LatestAdventuresComponent} from './components/adventures/latest-adventures/latest-adventures.component';
import {AdventureComponent} from './components/adventures/adventure/adventure.component';
import {LatestExpeditionsComponent} from './components/expeditions/latest-expeditions/latest-expeditions.component';
import {ExpeditionComponent} from './components/expeditions/expedition/expedition.component';
import {MenuProfileComponent} from './components/menu-profile/menu-profile.component';
import {ProgressBarModule} from 'primeng/progressbar';
import {QuestionComponent} from './components/question/question.component';
import {ToastModule} from 'primeng/toast';
import {ListExercicesComponent} from './components/list-exercices/list-exercices.component';
import {ExerciseInformationsComponent} from './components/exercise-informations/exercise-informations.component';
import {RouterModule} from '@angular/router';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {FormErrorsDisplayComponent} from './components/form-errors-display/form-errors-display.component';
import {MessageModule} from 'primeng/message';
import {ResponseDialogComponent} from './components/response-dialog/response-dialog.component';
import {DialogModule} from 'primeng/dialog';
import { ExpeditionFightComponent } from './components/expedition-fight/expedition-fight.component';
import { QcmQuestionComponent } from './components/question/qcm-question/qcm-question.component';
import { InputQuestionComponent } from './components/question/input-question/input-question.component';
import {TableModule} from 'primeng/table';
import {TooltipModule} from "primeng/tooltip";
import { AllExpeditionsComponent } from './components/expeditions/all-expeditions/all-expeditions.component';
import { ExpeditionsExerciseDialogComponent } from './components/expeditions/expeditions-exercise-dialog/expeditions-exercise-dialog.component';

@NgModule({
  declarations: [
    LatestAdventuresComponent,
    AdventureComponent,
    LatestExpeditionsComponent,
    ExpeditionComponent,
    ListExercicesComponent,
    ExerciseInformationsComponent,
    MenuProfileComponent,
    QuestionComponent,
    FormErrorsDisplayComponent,
    ExpeditionFightComponent,
    ResponseDialogComponent,
    QcmQuestionComponent,
    InputQuestionComponent,
    AllExpeditionsComponent,
    ExpeditionsExerciseDialogComponent],

  imports: [
    CommonModule,
    CheckboxModule,
    FormsModule,
    ButtonModule,
    ProgressBarModule,
    ToastModule,
    RouterModule,
    ProgressSpinnerModule,
    MessageModule,
    DialogModule,
    TableModule,
    TooltipModule
  ],

  exports: [
    CommonModule,
    CheckboxModule,
    FormsModule,
    ButtonModule,
    LatestAdventuresComponent,
    LatestExpeditionsComponent,
    ListExercicesComponent,
    MenuProfileComponent,
    QuestionComponent,
    ExerciseInformationsComponent,
    FormErrorsDisplayComponent,
    ResponseDialogComponent,
    ExpeditionFightComponent,
    AllExpeditionsComponent
  ]
})
export class SharedModule {
}
