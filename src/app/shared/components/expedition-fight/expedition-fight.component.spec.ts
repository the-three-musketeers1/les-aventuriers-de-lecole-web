import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpeditionFightComponent } from './expedition-fight.component';

describe('ExpeditionFightComponent', () => {
  let component: ExpeditionFightComponent;
  let fixture: ComponentFixture<ExpeditionFightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpeditionFightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpeditionFightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
