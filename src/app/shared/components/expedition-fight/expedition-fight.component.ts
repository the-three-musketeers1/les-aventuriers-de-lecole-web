import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Exercise} from "../../models/exercise/exercise";
import {DamageQuestion} from "../../models/damage-question";
import {$} from "protractor";
import {CharacterInfoDto} from "../../models/character-info-dto";
import {AnimatedCharacterFactoryService} from "../../../core/factories/animated-character-factory.service";
import {AnimatedCharacter} from "../../models/animated-character";

@Component({
  selector: 'app-expedition-fight',
  templateUrl: './expedition-fight.component.html',
  styleUrls: ['./expedition-fight.component.css']
})
export class ExpeditionFightComponent implements OnInit {

  @ViewChild('animatedCharacter')
  animatedCharacterCanvas;

  @ViewChild('animatedMonsterCanvas')
  animatedMonsterCanvas;

  @Input()
  character: CharacterInfoDto;

  @Input()
  exercise :Exercise;
  mainLoop: any;

  animatedCharacter: AnimatedCharacter;
  animatedMonster: AnimatedCharacter;


  @Input()
  set damage(val:DamageQuestion) {
    if (val != undefined) {
      if (val.damage == 0) {
        this.animatedCharacter.currentPosition = this.animatedCharacter.takeDmgPosition;
        this.animatedMonster.currentPosition = this.animatedMonster.attackPosition;
        setTimeout(() => {
          this.animatedCharacter.currentPosition = this.animatedCharacter.waitingPosition;
          this.animatedMonster.currentPosition = this.animatedMonster.waitingPosition;
        },2000);
      } else {
        if (this.monsterLife - val.damage > 0) {
          this.monsterLife = this.monsterLife - val.damage;
        } else {
          this.monsterLife = 0;
        }
        this.frame = 0;
        this.animatedCharacter.currentPosition = this.animatedCharacter.attackPosition;
        this.animatedMonster.currentPosition = this.animatedMonster.takeDmgPosition;
        setTimeout(() => {
          this.animatedCharacter.currentPosition = this.animatedCharacter.waitingPosition;
          this.animatedMonster.currentPosition = this.animatedMonster.waitingPosition;
        },2000);
      }
    }
  }

  monsterLife = 5000;

  private ctx: CanvasRenderingContext2D;
  private monsterCtx: CanvasRenderingContext2D;
  private frame = 0;
  private monsterFrame = 0;

  constructor(private animatedCharacterFactory: AnimatedCharacterFactoryService) { }

  ngOnInit(): void {
    this.selectCharacter();
    this.animatedMonster = this.animatedCharacterFactory.getMonsterAnimatedCharacter();

    this.animatedCharacter.currentPosition = this.animatedCharacter.waitingPosition;
    this.animatedMonster.currentPosition = this.animatedMonster.waitingPosition;
    this.monsterLife = this.exercise.life;
  }
  public ngAfterViewInit() {
    const canvasEl: HTMLCanvasElement = this.animatedCharacterCanvas.nativeElement;
    this.ctx = canvasEl.getContext('2d')!;
    const monsterEl: HTMLCanvasElement = this.animatedMonsterCanvas.nativeElement;
    this.monsterCtx = monsterEl.getContext('2d');

    this.ctx.lineWidth = 3;
    this.ctx.lineCap = 'round';
    this.ctx.strokeStyle = '#000';
    this.monsterCtx.lineWidth = 3;
    this.monsterCtx.lineCap = 'round';
    this.monsterCtx.strokeStyle = '#000';
    this.drawcharacters();
  }

  private drawcharacters() {
    let characterImage = new Image();
    let monsterImage = new Image();

    characterImage.src = this.animatedCharacter.filePath;
    monsterImage.src = this.animatedMonster.filePath;

    this.mainLoop = setInterval(() => {
      this.frame += 96;
      this.monsterFrame += 193;
      if(this.frame > 375) {
        this.frame = 0;
      }
      if (this.monsterFrame > 660){
        this.monsterFrame = 0;
      }
      this.ctx.clearRect(0,0,300,300);
      this.monsterCtx.clearRect(0,0,300,300);
      this.ctx.drawImage(characterImage, this.animatedCharacter.currentPosition.x + this.frame,
        this.animatedCharacter.currentPosition.y,this.animatedCharacter.currentPosition.sw,
        this.animatedCharacter.currentPosition.sh,this.animatedCharacter.currentPosition.dx,
        this.animatedCharacter.currentPosition.dy,this.animatedCharacter.currentPosition.dw,
        this.animatedCharacter.currentPosition.dh);
      this.monsterCtx.drawImage(monsterImage, this.animatedMonster.currentPosition.x + this.monsterFrame,
        this.animatedMonster.currentPosition.y,this.animatedMonster.currentPosition.sw,
        this.animatedMonster.currentPosition.sh,this.animatedMonster.currentPosition.dx,
        this.animatedMonster.currentPosition.dy,this.animatedMonster.currentPosition.dw,
        this.animatedMonster.currentPosition.dh);

      if(this.frame > 375) {
        this.frame = 0;
      }
      if (this.monsterFrame > 660){
        this.monsterFrame = 0;
      }
    },300);
  }

  getCurrentLife() {
    return (this.monsterLife / this.exercise.life) * 100;
  }

  private selectCharacter() {
    switch (this.character.role) {
      case 'Mage' :
        this.animatedCharacter = this.animatedCharacterFactory.getMageAnimatedCharacter();
        break;
      case 'Guerrier' :
        this.animatedCharacter = this.animatedCharacterFactory.getWarriorAnimatedCharacter();
        break;
      case 'Soigneur' :
        this.animatedCharacter = this.animatedCharacterFactory.getHealerAnimatedCharacter();
        break;
      case 'Voleur' :
        this.animatedCharacter = this.animatedCharacterFactory.getThiefAnimatedCharacter();
        break;
      default:
        break;
    }
  }
}
