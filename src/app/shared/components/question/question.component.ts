import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Question} from '../../models/question/question';
import {DamageQuestion} from "../../models/damage-question";
import {Answer} from "../../models/answer";
import {SelectedAnswer} from "../../models/selected-answer";
import {CharacterService} from "../../../core/services/character/character.service";
import {CharacterInfoDto} from "../../models/character-info-dto";

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  currentQuestion: Question;

  @Output()
  damage = new EventEmitter<DamageQuestion>();

  @Output()
  exerciseMark = new EventEmitter<SelectedAnswer[]>();


  @Input()
  questions: Question[] = [];
  barValueRemaining: number;
  timeValueRemaining: number;
  timeValueMax: number;
  index = 0;
  selectedAnswer:Answer = null;
  isQuestionVisible = false;
  answers: SelectedAnswer[] = [];

  interval: any;
  thiefAdviceActived: boolean;
  thiefAdviceContent = '';

  bonusConsumed = false;
  healerSpellUsed = false;

  characterValue: CharacterInfoDto;
  @Output()
  characterChange = new EventEmitter<CharacterInfoDto>();
  @Input()
  get character() { return this.characterValue}
  set character(val) {
    this.characterValue = val;
    this.characterChange.emit(this.characterValue);
  }

  constructor() { }

  ngOnInit(): void {

    this.currentQuestion = this.questions[this.index];
    this.barValueRemaining = 100;
    this.timeValueRemaining = this.currentQuestion.time;
    this.timeValueMax = this.currentQuestion.time;
    this.isQuestionVisible = true;
    this.startQuestion();
  }

  startQuestion() {
    let repetitions = 0;
    this.selectedAnswer = null;
    this.interval = setInterval(() => {
      this.barValueRemaining = this.barValueRemaining - (100 * 0.5 / this.timeValueMax);
      if (repetitions % 2 === 0 && repetitions > 1) {
        this.timeValueRemaining -= 1;
      }
      repetitions++;
      if (this.timeValueRemaining <= 0) {
        this.barValueRemaining = 0;
        clearInterval(this.interval);
        this.isQuestionVisible = false;
        this.addAnswer();
        this.checkNewQuestion();
      }
    }, 500);
  }

  private checkNewQuestion() {
    this.index++;
    if (this.index < this.questions.length) {
      setTimeout(() => {
        this.currentQuestion = this.questions[this.index];
        this.barValueRemaining = 100;
        this.timeValueRemaining = this.currentQuestion.time;
        this.timeValueMax = this.currentQuestion.time;
      }, 2000);
      setTimeout(() => {
        this.isQuestionVisible = true;
        this.bonusConsumed = false;
        if (this.healerSpellUsed === true) {
          this.bonusConsumed = true;
          this.healerSpellUsed = false;
        }
        this.startQuestion();
      } , 5000);
    } else {
      this.exerciseMark.emit(this.answers);
    }
    }

  private addAnswer() {
    if (this.selectedAnswer !== undefined &&  this.selectedAnswer !== null) {
      this.selectedAnswer.damage = this.questions[this.index].damage;
      this.answers.push({answer :this.selectedAnswer,questionId: this.questions[this.index].id,bonusConsumed: this.bonusConsumed });
      if (this.selectedAnswer.correct === true) {
        this.sendDamage(this.selectedAnswer.damage);
      } else {
        this.sendDamage(0);
      }
    } else {
      const emptyAnswer: Answer = {id:null, content:null, correct:false, damage:0};
      this.answers.push({answer :emptyAnswer ,questionId: this.questions[this.index].id,bonusConsumed: this.bonusConsumed });
      this.sendDamage(0)
    }
  }

  validQuestion() {
    this.isQuestionVisible = false;
    clearInterval(this.interval);
    this.addAnswer();
    this.checkNewQuestion();
  }

  sendDamage(dmg:number) {
    this.damage.emit({damage:dmg,questionId:this.currentQuestion.id});
  }

  castSpell() {
    if (this.character.action_points <= this.character.guild_bonus_cost || this.bonusConsumed === true) {
      return  ;
    }
    switch (this.character.role) {
      case 'Mage' :
        this.mageSpell();
        break;
      case 'Guerrier' :
        this.warriorSpell();
        break;
      case 'Soigneur' :
        this.healerSpell();
        break;
      case 'Voleur' :
        this.thiefSpell();
        break;
      default:
        break;
    }
    this.bonusConsumed = true;
    this.character.action_points -= this.character.guild_bonus_cost;
  }

  private warriorSpell() {
    const answersLength = this.currentQuestion.answers.length;
    const answersToRemove = Math.round(answersLength/2);
    this.currentQuestion.answers.forEach(answer => {
      if (answer.correct == false && answersToRemove > 0) {
        let idx = this.currentQuestion.answers.indexOf(answer);
        this.currentQuestion.answers.splice(idx,1);
      }
    });
  }

  private mageSpell() {
    this.timeValueRemaining += this.currentQuestion.time;
    this.timeValueMax += this.currentQuestion.time;
    this.barValueRemaining = this.timeValueRemaining / this.timeValueMax * 100;
  }

  private thiefSpell() {
    if (this.selectedAnswer !== null && this.selectedAnswer !== undefined && this.selectedAnswer.correct === true) {
      this.thiefAdviceContent = "Ton intuition semble bonne mon cher voleur ! "
    } else {
      this.thiefAdviceContent = "Tu es certains d'être un voleur ?!"
    }
    this.thiefAdviceActived = true;
    setTimeout(() => {
      this.thiefAdviceActived = false;
    },5000)
  }

  private healerSpell() {

    const lastAnswer = this.answers[this.answers.length - 1];
    if (lastAnswer.answer.correct === true ) {
      this.sendDamage(-lastAnswer.answer.damage);
    }

    this.index -= 2;
    this.answers.splice(-1,1);
    this.isQuestionVisible = false;
    clearInterval(this.interval);
    this.checkNewQuestion();
    this.healerSpellUsed = true;
   }

  spellDisabled() {
    if (this.bonusConsumed == true) {
      return true;
    }
    let roleCondition = true;
    switch (this.character.role ) {
      case 'Soigneur' :
        if (this.index == 0) {
          roleCondition = false;
        }
        if (this.answers.length > 0 && (this.answers[this.answers.length - 1].bonusConsumed === true || this.answers[this.answers.length - 1].answer.correct === true)) {
          roleCondition = false;
        }
        break;
      case 'Voleur' :
        if ( (this.currentQuestion.questionType === "qcm" && this.selectedAnswer === null) || this.currentQuestion.questionType === "input") {
          roleCondition = false;
        }
        break;
      default:
        break;
    }
    return !(this.character.action_points >= this.character.guild_bonus_cost && roleCondition);
  }

}
