import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Answer} from "../../../models/answer";
import {Question} from "../../../models/question/question";
import {log} from "util";

@Component({
  selector: 'app-input-question',
  templateUrl: './input-question.component.html',
  styleUrls: ['./input-question.component.scss']
})
export class InputQuestionComponent implements OnInit {

  @Input()
  isQuestionVisible : boolean;

  selectedAnswerValue : Answer;
  @Output() selectedAnswerChange = new EventEmitter<Answer>();
  @Input()
  get selectedAnswer() {return this.selectedAnswerValue };
  set selectedAnswer(val) {
    this.selectedAnswerValue = val;
    this.selectedAnswerChange.emit(val);
  }

  @Output() onValidQuestion = new EventEmitter<any>();

  @Input()
  currentQuestion: Question;
  inputUser: string;


  constructor() { }

  ngOnInit(): void {
  }

  findMatchingAnswer() {
    this.selectedAnswer = this.currentQuestion.answers.filter(answer => answer.content.trim().toUpperCase() === this.inputUser.trim().toUpperCase() && answer.correct === true)[0];
  }
  validQuestion() {
    this.onValidQuestion.emit();
  }
}
