import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Question} from "../../../models/question/question";
import {Answer} from "../../../models/answer";

@Component({
  selector: 'app-qcm-question',
  templateUrl: './qcm-question.component.html',
  styleUrls: ['./qcm-question.component.scss']
})
export class QcmQuestionComponent implements OnInit {

  @Input()
  isQuestionVisible : boolean;

  selectedAnswerValue : Answer;
  @Output() selectedAnswerChange = new EventEmitter<Answer>();
  @Input()
  get selectedAnswer() {return this.selectedAnswerValue };
  set selectedAnswer(val) {
    this.selectedAnswerValue = val;
    this.selectedAnswerChange.emit(val);
  }

  @Output() onValidQuestion = new EventEmitter<any>();

  @Input()
  currentQuestion: Question;

  constructor() { }

  ngOnInit(): void {
  }

  validQuestion() {
    this.onValidQuestion.emit();
  }

  selectAnswer(answer: Answer) {
      this.selectedAnswer = answer;
  }
}
