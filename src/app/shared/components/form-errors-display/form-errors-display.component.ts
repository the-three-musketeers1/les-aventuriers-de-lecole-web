import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormGroup, ValidationErrors} from '@angular/forms';

@Component({
  selector: 'app-form-errors-display',
  templateUrl: './form-errors-display.component.html',
  styleUrls: ['./form-errors-display.component.scss']
})
export class FormErrorsDisplayComponent implements OnInit {

  @Input() showErrors = false;
  @Input() errors: string[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }
}
