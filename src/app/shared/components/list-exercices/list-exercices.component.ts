import {Component, Input, OnInit} from '@angular/core';
import {Exercise} from '../../models/exercise/exercise';
import {ExerciseWithCharacterData} from "../../models/exercise/exercise-with-character-data";

@Component({
  selector: 'app-list-exercices',
  templateUrl: './list-exercices.component.html',
  styleUrls: ['./list-exercices.component.css']
})
export class ListExercicesComponent implements OnInit {

  private exercisesValue: ExerciseWithCharacterData[];
  @Input()
  get exercises() { return this.exercisesValue; }
  set exercises(val) {
    this.exercisesValue = val;
  }

  @Input()
  expeditionId: number;

  constructor() { }


  ngOnInit(): void {
  }

  canDisplayDescription(exercise) {
    return exercise.description.length > 0;
  }
}
