import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-response-dialog',
  templateUrl: './response-dialog.component.html',
  styleUrls: ['./response-dialog.component.scss']
})
export class ResponseDialogComponent implements OnInit, OnChanges {
  @Input() displayModal = false;
  @Input() isSuccess = false;
  @Input() successNext: string;
  @Input() failNext: string;
  @Input() statusCode: number;
  @Input() successDescription = 'Bravo ! Votre opération s\'est bien déroulée';
  @Input() customErrors = {};
  private preparedErrors = {
    400: 'Votre demande n\'est pas autorisé. Si l\'erreur persiste, veuillez contacter l\'administrateur',
    401: 'Vous n\'êtes pas authentifié(e), veuillez vous connecter',
    403: 'Vous n\'êtes pas autorisé à effectuer l\'action en cours',
    404: 'Il y a un problème de ressources manquantes, veuillez vérifier votre url et/ou réitérer l\'opération',
    500: 'Oups ! Un problème est survenu. Veuillez réessayer ultérieurement'
  };

  errorMessage: string;

  constructor(
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.customErrors[this.statusCode] === undefined &&
      this.preparedErrors[this.statusCode] === undefined) {
      this.statusCode = 500;
      this.errorMessage = this.preparedErrors['500'];
      return;
    }
    if (this.customErrors[this.statusCode]) {
      this.errorMessage = this.customErrors[this.statusCode];
      return;
    }
    this.errorMessage = this.preparedErrors[this.statusCode];
  }

  async action() {
    this.displayModal = false;
    if (this.isSuccess && this.successNext) {
      await this.router.navigate([this.successNext]);
      return;
    }
    if (this.failNext) {
      await this.router.navigate([this.failNext]);
    }
  }
}
