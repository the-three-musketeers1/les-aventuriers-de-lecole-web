import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ResponseDialogComponent} from './response-dialog.component';

describe('ResponseDialogComponent', () => {
  let responseDialogComponent: ResponseDialogComponent;
  let mockRouter;

  beforeEach(async(() => {
    mockRouter = jasmine.createSpyObj('Router', ['navigate']);
    responseDialogComponent = new ResponseDialogComponent(mockRouter);
  }));

  it('should create', () => {
    expect(responseDialogComponent).toBeTruthy();
  });

  it('should have default values', () => {
    expect(responseDialogComponent.displayModal).toBe(false);
    expect(responseDialogComponent.isSuccess).toBe(false);
    expect(responseDialogComponent.successNext).toBe(undefined);
    expect(responseDialogComponent.failNext).toBe(undefined);
    expect(responseDialogComponent.statusCode).toBe(undefined);
    expect(responseDialogComponent.successDescription).toBe('Bravo ! Votre opération s\'est bien déroulée');
  });

  describe('method ngOnChanges', () => {
    let changes;
    beforeEach(() => {
      changes = jasmine.createSpyObj('SimpleChanges', ['']);
    });
    it('should affect status code 500 when is not known case', () => {
      responseDialogComponent.statusCode = 418;
      responseDialogComponent.ngOnChanges(changes);

      expect(responseDialogComponent.statusCode).toBe(500);
    });
    it('should affect default message', () => {
      const defaultErrorMessage = 'Oups ! Un problème est survenu. Veuillez réessayer ultérieurement';
      responseDialogComponent.statusCode = 418;

      responseDialogComponent.ngOnChanges(changes);

      expect(responseDialogComponent.errorMessage).toBe(defaultErrorMessage);
    });
    [
      {statusCode: 401, errorMessage: 'Vous n\'êtes pas authentifié(e), veuillez vous connecter'},
      {statusCode: 403, errorMessage: 'Vous n\'êtes pas autorisé à effectuer l\'action en cours'},
      {statusCode: 404, errorMessage: 'Nous n\'avons pas trouvé votre page'},
      {
        statusCode: 400,
        errorMessage: 'Votre demande n\'est pas autorisé. Si l\'erreur persiste, veuillez contacter l\'administrateur'
      }
    ]
      .forEach(data => {
        it(`should keep status ${data.statusCode}, and affect proper error message`, () => {
          responseDialogComponent.statusCode = data.statusCode;

          responseDialogComponent.ngOnChanges(changes);
          expect(responseDialogComponent.statusCode).toBe(data.statusCode);
          expect(responseDialogComponent.errorMessage).toBe(data.errorMessage);
        });
      });
    it('should show custom error messages', () => {
      responseDialogComponent.customErrors = {
        404: 'Custom error message bro'
      };
      responseDialogComponent.statusCode = 404;

      responseDialogComponent.ngOnChanges(changes);

      expect(responseDialogComponent.errorMessage).toBe('Custom error message bro');
    });
  });

  describe('method action', () => {
    beforeEach(() => {
      responseDialogComponent.displayModal = true;
    });
    it('should close modal', async () => {
      expect(responseDialogComponent.displayModal).toBe(true);
      await responseDialogComponent.action();
      expect(responseDialogComponent.displayModal).toBe(false);
    });
    it('should not route when response fail and failNext empty', async () => {
      expect(responseDialogComponent.failNext).toBe(undefined);
      expect(responseDialogComponent.isSuccess).toBe(false);

      await responseDialogComponent.action();

      expect(mockRouter.navigate).not.toHaveBeenCalled();
    });
    it('should not route when response success and successNext empty', async () => {
      responseDialogComponent.isSuccess = true;
      expect(responseDialogComponent.successNext).toBe(undefined);

      await responseDialogComponent.action();

      expect(mockRouter.navigate).not.toHaveBeenCalled();
    });
    it('should route when response is success and next success route is indicated', async () => {
      responseDialogComponent.successNext = '/success-next';
      responseDialogComponent.isSuccess = true;

      await responseDialogComponent.action();

      expect(mockRouter.navigate).toHaveBeenCalledTimes(1);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['/success-next']);
    });
    it('should route when response is fail and next fail route is indicated', async () => {
      responseDialogComponent.failNext = '/fail-next';
      responseDialogComponent.isSuccess = false;

      await responseDialogComponent.action();

      expect(mockRouter.navigate).toHaveBeenCalledTimes(1);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['/fail-next']);
    });
    it('should route one time when response is success and next success and fail are indicated', async () => {
      responseDialogComponent.successNext = '/success-next';
      responseDialogComponent.failNext = '/fail-next';
      responseDialogComponent.isSuccess = true;

      await responseDialogComponent.action();

      expect(mockRouter.navigate).toHaveBeenCalledTimes(1);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['/success-next']);
    });
  });
});
