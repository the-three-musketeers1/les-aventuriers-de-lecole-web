import {Component, Input, OnInit} from '@angular/core';
import {CharacterService} from "../../../core/services/character/character.service";
import {CharacterInfoDto} from "../../models/character-info-dto";
import {RolePictureService} from "../../../core/services/role-picture.service";

@Component({
  selector: 'app-menu-profile',
  templateUrl: './menu-profile.component.html',
  styleUrls: ['./menu-profile.component.css']
})
export class MenuProfileComponent implements OnInit {

  @Input()
  character: CharacterInfoDto;
  max_experience: number = 2000;

  constructor(private rolePictureService: RolePictureService) { }

  ngOnInit(): void {
  }

  getCharacterExperience() {
    return this.character.experience * 100 / this.max_experience;
  }

  get rolePicturePath() {
    if (this.character !== undefined) {
      return this.rolePictureService.getRolePicturePathByRoleName(this.character.role);
    }
    return '';
  }
}
