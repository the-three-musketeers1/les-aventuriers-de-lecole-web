import {Component, Input, OnInit} from '@angular/core';
import {AdventureInformation} from '../../../models/adventure-information/adventure-information';

@Component({
    selector: 'app-adventure',
    templateUrl: './adventure.component.html',
    styleUrls: ['./adventure.component.css']
  })
  export class AdventureComponent implements OnInit {

  private adventureValue: AdventureInformation;
  @Input()
  get adventure() { return this.adventureValue; }
  set adventure(val) {
    this.adventureValue = val;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
