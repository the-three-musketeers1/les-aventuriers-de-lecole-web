import { Component, OnInit } from '@angular/core';
import {AdventureInformation} from '../../../models/adventure-information/adventure-information';

@Component({
  selector: 'app-latest-adventures',
  templateUrl: './latest-adventures.component.html',
  styleUrls: ['./latest-adventures.component.css']
})
export class LatestAdventuresComponent implements OnInit {
  availableAdventures: AdventureInformation[] = [];

  constructor() {
  }

  ngOnInit(): void {
    this.availableAdventures.push(new AdventureInformation('Premier donjon', 'Mathématique', 'Thalès'));
    this.availableAdventures.push(new AdventureInformation('Deuxième donjon', 'Français', 'COD'));
  }

}
