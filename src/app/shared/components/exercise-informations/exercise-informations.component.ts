import {Component, Input, OnInit} from '@angular/core';
import {Exercise} from '../../models/exercise/exercise';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-exercise-information',
  templateUrl: './exercise-informations.component.html',
  styleUrls: ['./exercise-informations.component.css']
})
export class ExerciseInformationsComponent implements OnInit {

  private exerciseValue: Exercise;
  expeditionId : number;
  @Input()
  get exercise() { return this.exerciseValue; }
  set exercise(val) {
    this.exerciseValue = val;
  }



  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {

      if (params.get('expeditionId')){
        this.expeditionId = parseInt(params.get('expeditionId'));
      }
        });
    }

}
