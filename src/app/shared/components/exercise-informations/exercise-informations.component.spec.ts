import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseInformationsComponent } from './exercise-informations.component';

describe('ExerciceComponent', () => {
  let component: ExerciseInformationsComponent;
  let fixture: ComponentFixture<ExerciseInformationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExerciseInformationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExerciseInformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
