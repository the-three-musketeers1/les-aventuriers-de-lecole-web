import {Component, Input, OnInit} from '@angular/core';
import {ExpeditionInformation} from '../../../models/expedition-information/expedition-information';
import {DateUtilsService} from "../../../utils/date-utils.service";

@Component({
  selector: 'app-expedition',
  templateUrl: './expedition.component.html',
  styleUrls: ['./expedition.component.css']
})
export class ExpeditionComponent implements OnInit {

  private expeditionValue: ExpeditionInformation;
  @Input()
  get expedition() { return this.expeditionValue; }
  set expedition(val) {
    this.expeditionValue = val;
  }

  constructor(private dateUtils: DateUtilsService) { }

  ngOnInit(): void {
  }

}
