import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpeditionsExerciseDialogComponent } from './expeditions-exercise-dialog.component';

describe('ExpeditionsExerciseDialogComponent', () => {
  let component: ExpeditionsExerciseDialogComponent;
  let fixture: ComponentFixture<ExpeditionsExerciseDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpeditionsExerciseDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpeditionsExerciseDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
