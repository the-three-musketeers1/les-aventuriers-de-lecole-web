import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ExpeditionInformation} from "../../../models/expedition-information/expedition-information";
import {ExerciseWithCharacterData} from "../../../models/exercise/exercise-with-character-data";
import {ExercicesService} from "../../../../core/services/exercices.service";

@Component({
  selector: 'app-expeditions-exercise-dialog',
  templateUrl: './expeditions-exercise-dialog.component.html',
  styleUrls: ['./expeditions-exercise-dialog.component.scss']
})
export class ExpeditionsExerciseDialogComponent implements OnInit {

  @Input()
  displayDialog: boolean = false;
  @Output()
  onCloseDialog = new EventEmitter<any>();

  expeditionValue : ExpeditionInformation;
  @Input()
  set expedition(val) {
    this.expeditionValue = val;
    this.loadExercise()
  }
  get expedition() { return this.expeditionValue }

  exercises: ExerciseWithCharacterData[];

  constructor(private exerciseService : ExercicesService) { }

  ngOnInit(): void {
  }

  hideDialog() {
    this.onCloseDialog.emit();
  }

  canDisplayDescription(exercise) {
    return exercise.description.length > 0;
  }

  loadExercise() {
    let characterId = localStorage.getItem('characterId');
    this.exerciseService.getExercicesOfExpeditionsWithCharacterData(this.expedition.id,Number.parseInt(characterId)).then(result => {
      this.exercises = result;
    });


  }
}
