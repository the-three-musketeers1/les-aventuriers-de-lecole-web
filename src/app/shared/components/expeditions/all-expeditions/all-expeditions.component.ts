import { Component, OnInit } from '@angular/core';
import {ExpeditionService} from "../../../../core/services/expedition/expedition.service";
import {DateUtilsService} from "../../../utils/date-utils.service";
import {ExpeditionInformation} from "../../../models/expedition-information/expedition-information";

@Component({
  selector: 'app-all-expeditions',
  templateUrl: './all-expeditions.component.html',
  styleUrls: ['./all-expeditions.component.scss']
})
export class AllExpeditionsComponent implements OnInit {

  expeditions: ExpeditionInformation[] = [];

  selectedExpedition: ExpeditionInformation = null ;

  displaydialog: boolean;

  constructor(
    private expeditionService: ExpeditionService,
    private dateUtils: DateUtilsService
  ) { }

  ngOnInit(): void {
    this.expeditionService.getAllExpeditionsByCharacter().then(expeditions => {
      expeditions.forEach(expedition => {
        this.expeditions.push(expedition);
      });
    });
  }

  getExpeditionDisciplines(expeditionData: ExpeditionInformation) {
    return expeditionData.disciplines.join('/ ');
  }

  getExpeditionDate(expeditionData: ExpeditionInformation) {
    return this.dateUtils.getDisplayableFrenchDateWithoutTime(expeditionData.deadline);
  }

  canDisplayDescription(expeditionData: ExpeditionInformation) {
    return expeditionData.description.length > 0;
  }

  showExercisesDialog(expeditionData: ExpeditionInformation) {
    this.selectedExpedition = expeditionData;
    this.displaydialog = true;
  }

  closeDialog() {
    this.selectedExpedition = null;
    this.displaydialog = false;
  }
}
