import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllExpeditionsComponent } from './all-expeditions.component';

describe('AllExpeditionsComponent', () => {
  let component: AllExpeditionsComponent;
  let fixture: ComponentFixture<AllExpeditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllExpeditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllExpeditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
