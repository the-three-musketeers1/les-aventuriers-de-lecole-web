import { Component, OnInit } from '@angular/core';
import {ExpeditionInformation} from '../../../models/expedition-information/expedition-information';
import {ExpeditionService} from "../../../../core/services/expedition/expedition.service";
import {DateUtilsService} from "../../../utils/date-utils.service";

@Component({
  selector: 'app-latest-expeditions',
  templateUrl: './latest-expeditions.component.html',
  styleUrls: ['./latest-expeditions.component.css']
})
export class LatestExpeditionsComponent implements OnInit {
  availableExpeditions: ExpeditionInformation[] = [];

  constructor(
    private expeditionService: ExpeditionService,
    private dateUtils: DateUtilsService
  ) { }

  ngOnInit(): void {
    this.expeditionService.getAvailableExpeditionsByCharacter().then(expeditions => {
      expeditions.forEach(expedition => {
        this.availableExpeditions.push(expedition);
      });
    });
  }

  getExpeditionDisciplines(expeditionData: ExpeditionInformation) {
    return expeditionData.disciplines.join('/ ');
  }

  getExpeditionDate(expeditionData: ExpeditionInformation) {
    return this.dateUtils.getDisplayableFrenchDateWithoutTime(expeditionData.deadline);
  }

  canDisplayDescription(expeditionData: ExpeditionInformation) {
    return expeditionData.description.length > 0;
  }
}
