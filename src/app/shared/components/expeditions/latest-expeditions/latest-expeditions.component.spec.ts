import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LatestExpeditionsComponent } from './latest-expeditions.component';

describe('LatestExpeditionsComponent', () => {
  let component: LatestExpeditionsComponent;
  let fixture: ComponentFixture<LatestExpeditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LatestExpeditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LatestExpeditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
