import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './home/home.component';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    SharedModule,
    CardModule,
    ButtonModule,
    RouterModule
  ]
})
export class HomeModule { }
