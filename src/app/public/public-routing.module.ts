import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home/home.component';
import {AuthFormComponent} from './auth-form/auth-form.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'subscribe', component: AuthFormComponent },
  { path: 'login', component: AuthFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
