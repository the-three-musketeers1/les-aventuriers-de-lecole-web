import {NgModule} from '@angular/core';
import {SharedModule} from 'src/app/shared/shared.module';
import {PublicRoutingModule} from './public-routing.module';
import {HomeModule} from './home/home.module';
import {AuthFormComponent} from './auth-form/auth-form.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {PasswordModule} from 'primeng/password';
import {MessageModule} from 'primeng/message';
import {DialogModule} from 'primeng/dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  declarations: [AuthFormComponent],
  imports: [
    SharedModule,
    PublicRoutingModule,
    HomeModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    PasswordModule,
    MessageModule,
    DialogModule,
    BrowserAnimationsModule,
  ]
})
export class PublicModule {
}
