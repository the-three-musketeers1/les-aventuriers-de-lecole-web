import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {AuthService} from '../../core/services/auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-subscribe-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.scss'],
})
export class AuthFormComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public authService: AuthService
  ) {
  }

  get checkErrors() {
    if (!this.showErrors) {
      return false;
    }
    this.errors = [];
    this.errors = this.fillErrors();

    return (this.errors.length > 0);
  }
  authForm: FormGroup;
  formTitle: string;
  buttonLabel: string;
  failNext = '/';
  errors: string[] = [];
  translateProperty = {firstname: 'Prénom', lastname: 'Nom', email: 'Email', password: 'Mot de passe'};
  showErrors = false;
  displayModal = false;
  isSuccess = false;
  isSubscribeForm: boolean;
  statusCode = 0;
  customErrors = {};

  private static fillControlsConfig() {
    return {
      firstname: ['', [
        Validators.required,
        Validators.pattern('(^\\S+.*\\S+$|^\\S+$)')
      ]],
      lastname: ['', [
        Validators.required,
        Validators.pattern('(^\\S+.*\\S+$|^\\S+$)')
      ]],
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required,
        Validators.pattern('^[^\\s]+$'),
        Validators.minLength(4)
      ]]
    };
  }

  async ngOnInit(): Promise<void> {
    this.isSubscribeForm  = this.router.url.slice(1) === 'subscribe';
    this.formTitle = (this.isSubscribeForm) ? 'Créer votre compte' : 'Connexion';
    this.buttonLabel = (this.isSubscribeForm) ? 'Créer' : 'Valider';

    const controlsConfig = AuthFormComponent.fillControlsConfig();

    this.failNext = undefined;

    if (!this.isSubscribeForm) {
      delete controlsConfig.firstname;
      delete controlsConfig.lastname;
      this.customErrors =  {
        404: 'Mot de passe ou email incorrect'
      };
    }

    this.authForm = this.formBuilder.group(controlsConfig);

    if (this.authService.isLoggedIn) {
      await this.authService.logout();
    }
  }

  async onSubmit() {
    if (this.authForm.invalid) {
      this.showErrors = true;
      return;
    }
    try {
      this.displayModal = false;
      this.showErrors = false;
      if (this.isSubscribeForm) {
        await this.authService.subscribe(this.authForm.value);
        this.isSuccess = true;
      } else {
        await this.authService.login(this.authForm.value);
        await this.router.navigate(['/']);
      }
      this.authForm.reset();
    } catch (err) {
      this.statusCode = err.status;
      this.isSuccess = false;
    } finally {
      this.displayModal = true;
    }
  }

  private fillErrors() {
    Object.keys(this.authForm.value).forEach(property => {
      const currentFromControl = this.authForm.get(property);
      if (currentFromControl.errors) {
        const errorSentence = this.getErrorSentence(this.translateProperty[property], currentFromControl.errors);
        this.errors.push(errorSentence);
      }
    });

    return this.errors;
  }

  private getErrorSentence(translateProperty: string, errors: ValidationErrors): string {
    let endSentence = '';
    if (errors.required) {
      endSentence = 'est obligatoire';
    }
    if (errors.minlength) {
      endSentence = 'doit avoir une longueur minimum';
      if (errors.minlength.requiredLength) {
        endSentence += ` de ${errors.minlength.requiredLength} charactères`;
      }
    }
    if (errors.email) {
      endSentence = `doit être un email`;
    }
    if (errors.pattern) {
      if (errors.pattern.requiredPattern === '^(^\\S+.*\\S+$|^\\S+$)$') {
        endSentence = 'doit commencer et terminer par un caractère non blanc';
      }
      if (errors.pattern.requiredPattern === '^[^\\s]+$') {
        endSentence = 'ne doit pas contenir d\'espace';
      }
    }

    if (endSentence.length === 0) {
      endSentence = 'n\'est pas valide';
    }

    return `Le champ '${translateProperty}' ${endSentence}`;
  }
}
