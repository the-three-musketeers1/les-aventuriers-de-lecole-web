import {AuthFormComponent} from './auth-form.component';

describe('SubscribeFormComponent', () => {
  let authFormComponent: AuthFormComponent;
  let mockFormGroup: any;
  let mockFormBuilder: any;
  let mockAuthService;
  let mockRouter;

  beforeEach(() => {
    mockFormGroup = jasmine.createSpyObj('FormGroup', ['get', 'reset']);
    mockFormGroup.invalid = false;
    mockFormBuilder = jasmine.createSpyObj('FormBuilder', ['group']);
    mockFormBuilder.group.and.returnValue(mockFormGroup);
    mockAuthService = jasmine.createSpyObj('AuthService', ['subscribe', 'logout']);
    mockRouter = jasmine.createSpyObj('Router', ['navigate']);

    authFormComponent = new AuthFormComponent(mockFormBuilder, mockRouter, mockAuthService);
    authFormComponent.authForm = mockFormGroup;
  });

  it('should create authFormComponent', () => {
    expect(authFormComponent).toBeDefined();
  });

  describe('method ngOnInit', () => {
    it('should be call group method of form builder', async () => {
      mockRouter.url = '/subscribe';
      await authFormComponent.ngOnInit();
      expect(mockFormBuilder.group).toHaveBeenCalledTimes(1);
    });
    it('should define subscribe form', async () => {
      mockRouter.url = '/subscribe';
      await authFormComponent.ngOnInit();
      expect(authFormComponent.authForm).toBeDefined();
      expect(authFormComponent.isSubscribeForm).toBe(true);
      expect(authFormComponent.formTitle).toBe('Créer votre compte');
      expect(authFormComponent.buttonLabel).toBe('Créer');
      expect(authFormComponent.failNext).toBe('/');
    });
    it('should define login form', async () => {
      mockRouter.url = '/login';
      await authFormComponent.ngOnInit();
      expect(authFormComponent.authForm).toBeDefined();
      expect(authFormComponent.isSubscribeForm).toBe(false);
      expect(authFormComponent.formTitle).toBe('Connexion');
      expect(authFormComponent.buttonLabel).toBe('Valider');
    });
    it('should affect custom errors when is not subscribe form', async () => {
      mockRouter.url = '/login';
      await authFormComponent.ngOnInit();
      expect(authFormComponent.customErrors).toEqual({
        404: 'Mot de passe ou email incorrect'
      });
    });
    it('should not affect failNext when login is not define', async () => {
      mockRouter.url = '/login';
      await authFormComponent.ngOnInit();
      expect(authFormComponent.failNext).toBe(undefined);
    });
    it('should logout if user is login already', async () => {
      mockRouter.url = '/login';
      mockAuthService.isLoggedIn = true;

      await authFormComponent.ngOnInit();

      expect(mockAuthService.logout).toHaveBeenCalledTimes(1);
    });
  });

  describe('method onSubmit', () => {
    beforeEach(() => {
      mockAuthService.subscribe.and.returnValue(Promise.resolve('test'));
    });
    it('should show error when subscribe form is invalid', () => {
      expect(authFormComponent.showErrors).toBe(false);
      mockFormGroup.invalid = true;

      authFormComponent.onSubmit();

      expect(authFormComponent.showErrors).toBe(true);
    });
    it('should not show error when it\'s show already and form is valid', async () => {
      mockFormGroup.invalid = true;
      await authFormComponent.onSubmit();
      expect(authFormComponent.showErrors).toBe(true);

      mockFormGroup.invalid = false;
      await authFormComponent.onSubmit();
      expect(authFormComponent.showErrors).toBe(false);
    });
    it('should send request to subscribe with subscribe form data by auth service', async () => {
      authFormComponent.isSubscribeForm = true;
      mockFormGroup.invalid = false;
      await authFormComponent.onSubmit();
      expect(authFormComponent.authService.subscribe).toHaveBeenCalledTimes(1);
      expect(authFormComponent.authService.subscribe).toHaveBeenCalledWith(mockFormGroup.value);
    });
  });
});
