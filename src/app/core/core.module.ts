import {NgModule, Optional, SkipSelf} from '@angular/core';
import {PublicModule} from '../public/public.module';
import {ProtectedModule} from '../protected/protected.module';
import {NavbarComponent} from './components/navbar/navbar.component';
import {ToolbarModule} from 'primeng/toolbar';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './interceptors/auth.interceptor';
import {TicketDialogComponent} from './components/navbar/ticket-dialog/ticket-dialog.component';
import {DialogModule} from 'primeng/dialog';
import {InputTextModule} from "primeng/inputtext";
import {InputTextareaModule} from "primeng/inputtextarea";
import {ReactiveFormsModule} from "@angular/forms";
import {DropdownModule} from "primeng/dropdown";
import {ForbiddenComponent} from './components/errors/forbidden/forbidden.component';


@NgModule({
  declarations: [NavbarComponent, TicketDialogComponent, ForbiddenComponent],
  imports: [
    PublicModule,
    ProtectedModule,
    ToolbarModule,
    SharedModule,
    RouterModule,
    HttpClientModule,
    DialogModule,
    InputTextModule,
    InputTextareaModule,
    ReactiveFormsModule,
    DropdownModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  exports: [NavbarComponent]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded.');
    }
  }
}
