import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';

import {ExerciseWithCharacterData} from '../../shared/models/exercise/exercise-with-character-data';
import {ExercicesService} from "../services/exercices.service";

@Injectable({
  providedIn: 'root'
})
export class ExerciseExpeditionResolver implements Resolve<ExerciseWithCharacterData[] | boolean> {

  constructor(
    private router: Router,
    private exerciseService: ExercicesService
  ) {
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<ExerciseWithCharacterData[] | boolean> {
    try {
      const characterId = localStorage.getItem('characterId');
      const expeditionId = route.params.expeditionId;
      return await this.exerciseService.getExercicesOfExpeditionsWithCharacterData(expeditionId, characterId);
    } catch (e) {
      return await this.router.navigateByUrl('' + e.status, {skipLocationChange: true});
    }
  }
}
