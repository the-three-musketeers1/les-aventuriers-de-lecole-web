import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Message} from '../../shared/models/message/message';
import {Observable} from 'rxjs';
import {MessageService} from '../services/message/message.service';

@Injectable({
  providedIn: 'root'
})
export class TicketMessagesResolver implements Resolve<Message[]|boolean>{

  constructor(
    private router: Router,
    private messageService: MessageService
  ) { }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Message[] | boolean>{
    try {
      return await this.messageService.getTicketMessages(route.params.ticketId);
    } catch (e) {
      return await this.router.navigateByUrl('' + e.status, {skipLocationChange: true});
    }
  }
}
