import {TestBed} from '@angular/core/testing';

import {GuildResolver} from './guild.resolver';
import {Router} from '@angular/router';
import {GuildService} from '../services/guild/guild.service';

describe('GuildResolver', () => {
  let service: GuildResolver;
  let mockRouter;
  let mockGuildService;

  beforeEach(() => {
    mockRouter = jasmine.createSpyObj('Router', ['']);
    mockGuildService = jasmine.createSpyObj('GuildService', ['']);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Router,
          useValue: mockRouter
        },
        {
          provide: GuildService,
          useValue: mockGuildService
        }
      ]
    });
    service = TestBed.inject(GuildResolver);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
