import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {CharacterService} from '../services/character/character.service';
import {Character} from '../../shared/models/character/character';

@Injectable({
  providedIn: 'root'
})
export class CharacterStorageResolver implements Resolve<Character | boolean> {

  constructor(
    private router: Router,
    private characterService: CharacterService
  ) {
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Character | boolean> {
    try {
      const characterId = this.characterService.getIdInLocalStorage();
      if (characterId === undefined) {
        await this.router.navigateByUrl('characters');
        return false;
      }
      return await this.characterService.getCharacterStudent(+characterId);
    } catch (e) {
      this.characterService.removeIdInLocalStorage();
      return await this.router.navigateByUrl('' + e.status, {skipLocationChange: true});
    }
  }
}
