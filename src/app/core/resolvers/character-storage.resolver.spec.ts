import { TestBed } from '@angular/core/testing';

import { CharacterStorageResolver } from './character-storage.resolver';

describe('CharacterStorageService', () => {
  let service: CharacterStorageResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CharacterStorageResolver);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
