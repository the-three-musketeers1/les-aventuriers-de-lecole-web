import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Guild} from '../../shared/models/guild/guild';
import {GuildService} from '../services/guild/guild.service';

@Injectable({
  providedIn: 'root'
})
export class GuildResolver implements Resolve<Guild|boolean> {

  constructor(
    private router: Router,
    private guildService: GuildService
  ) {
  }

 async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Guild|boolean> {
    try {
      const guildId = route.params.guildId;
      const guildToken = route.params.guildToken;
      return await this.guildService.getGuildByIdAndToken(guildId, guildToken);
    } catch (e) {
      return await this.router.navigateByUrl('' + e.status, {skipLocationChange: true});
    }
  }
}
