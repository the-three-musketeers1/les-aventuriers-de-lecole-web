import { TestBed } from '@angular/core/testing';

import { TicketMessagesResolver } from './ticket.messages.resolver';
import {Router} from '@angular/router';
import {MessageService} from '../services/message/message.service';

describe('TicketMessageResolver', () => {
  let service: TicketMessagesResolver;
  let mockRouter;
  let mockMessageService;

  beforeEach(() => {
    mockRouter = jasmine.createSpyObj('Router', ['']);
    mockMessageService = jasmine.createSpyObj('MessageService', ['']);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Router,
          useValue: mockRouter
        },
        {
          provide: MessageService,
          useValue: mockMessageService
        }
      ]
    });
    service = TestBed.inject(TicketMessagesResolver);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
