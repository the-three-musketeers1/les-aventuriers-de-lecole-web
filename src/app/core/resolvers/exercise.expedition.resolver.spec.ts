import {TestBed} from '@angular/core/testing';
import {ExerciseExpeditionResolver} from './exercise.expedition.resolver';
import {Router} from '@angular/router';
import {ExercicesService} from "../services/exercices.service";

describe('ExerciseExpeditionResolver', () => {
  let service: ExerciseExpeditionResolver;
  let mockRouter: Router;
  let mockExercicesService: ExercicesService;

  beforeEach(() => {
    mockRouter = jasmine.createSpyObj('Router', ['']);
    mockExercicesService = jasmine.createSpyObj('ExercicesService', ['']);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Router,
          useValue: mockRouter
        },
        {
          provide: ExercicesService,
          useValue: mockExercicesService
        }
      ]
    });
    service = TestBed.inject(ExerciseExpeditionResolver);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
