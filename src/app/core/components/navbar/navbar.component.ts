import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls : ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  homePath: string;
  displayTicketDialog: boolean = false;

  constructor(
    public authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.homePath = this.authService.isLoggedIn ? 'home' : 'main';
  }

  isCharacterSelected() {
    return localStorage.getItem("characterId") != null;
  }

  async logout() {
    await this.authService.logout();
    this.homePath = 'main';
  }

  async login() {
    await this.router.navigate(['login']);
    this.homePath = 'home';
  }

  showTicketDialog() {
    this.displayTicketDialog = true;
  }

  hideTicketDialog() {
    this.displayTicketDialog = false;
  }
}
