import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CharacterService} from "../../../services/character/character.service";
import {TicketService} from "../../../services/ticket/ticket.service";

@Component({
  selector: 'app-ticket-dialog',
  templateUrl: './ticket-dialog.component.html',
  styleUrls: ['./ticket-dialog.component.scss']
})
export class TicketDialogComponent implements OnInit {
  @Input() display: boolean;
  @Output() closeDialog = new EventEmitter();

  ticketForm: FormGroup;
  sendSucceed: boolean = false;
  sendFailed: boolean = false;

  constructor(private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private characterService: CharacterService,
              private ticketService : TicketService) {}

   ngOnInit() {
    this.initForm();
  }

  initForm() {
      this.ticketForm = this.formBuilder.group({
          ticketTitle: new FormControl('', Validators.required),
          ticketMessage: new FormControl('', Validators.required)
      });
  }

  onCloseDialog() {
    this.closeDialog.emit(false);
  }

  onCloseSucceed() {
      this.sendSucceed = false;
      this.initForm();
  }

    onCloseFailed() {
        this.sendFailed = false;
    }

  async confirmTicket(value: string) {
    this.display = false;
    this.onCloseDialog();

    this.ticketService.saveTicket(value['ticketTitle'], value['ticketMessage']).then(res => {
        this.sendSucceed = true;
    }).catch(e => {
        this.sendFailed = true;
    });
  }

}
