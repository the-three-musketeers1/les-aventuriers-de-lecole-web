import { TestBed } from '@angular/core/testing';
import { AuthGuard } from './auth.guard';
import {AuthService} from '../services/auth/auth.service';
import {Router} from '@angular/router';

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let mockAuthService;
  let mockRouter;

  beforeEach(() => {
    mockAuthService = jasmine.createSpyObj('AuthService', ['']);
    mockRouter = jasmine.createSpyObj('Router', ['']);
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        {
          provide: AuthService,
          useValue: mockAuthService
        },
        {
          provide: Router,
          useValue: mockRouter
        }
      ]
    });
    guard = TestBed.inject(AuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
