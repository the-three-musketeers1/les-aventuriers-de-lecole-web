import { TestBed } from '@angular/core/testing';

import { CharacterGuard } from './character.guard';
import {CharacterService} from '../services/character/character.service';
import {Router} from '@angular/router';

describe('CharacterGuard', () => {
  let guard: CharacterGuard;
  let mockRouter;
  let mockCharacterService;

  beforeEach(() => {
    mockRouter = jasmine.createSpyObj('Router', ['']);
    mockCharacterService = jasmine.createSpyObj('CharacterService', ['']);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Router,
          useValue: mockRouter
        },
        {
          provide: CharacterService,
          useValue: mockCharacterService
        }
      ]
    });
    guard = TestBed.inject(CharacterGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
