import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {CharacterService} from '../services/character/character.service';

@Injectable({
  providedIn: 'root'
})
export class CharacterGuard implements CanActivate {

  constructor(
    public characterService: CharacterService,
    public router: Router
  ) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const characterIdStorage = this.characterService.getIdInLocalStorage();
    if (characterIdStorage === null) {
      this.router.navigate(['characters']).then(() => {});
      return false;
    }

    return true;
  }
}
