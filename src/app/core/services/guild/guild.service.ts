import {Injectable} from '@angular/core';
import {UrlFactoryService} from '../../factories/url-factory.service';
import {RequestService} from '../request.service';
import {Router} from '@angular/router';
import {Guild} from '../../../shared/models/guild/guild';

@Injectable({
  providedIn: 'root'
})
export class GuildService {

  constructor(
    private urlFactoryService: UrlFactoryService,
    private requestService: RequestService,
    public router: Router
  ) {
  }

  async getGuildByNameAndToken(guildName: string, guildToken: string) {
    const sendData = {
      name: guildName,
      token: guildToken
    };
    const guild = await this.requestService.get<Guild>(
      this.urlFactoryService.getGuildUrl() + `/student/?name=${guildName}&token=${guildToken}`
    );
    await this.router.navigate([`/form-character/${guild.id}/token/${guildToken}`]);
  }

  async getGuildByIdAndToken(guildId: number, guildToken: string) {
    return this.requestService.get<Guild>(
      this.urlFactoryService.getGuildUrl() + `/${guildId}/token/${guildToken}`
    );
  }
}
