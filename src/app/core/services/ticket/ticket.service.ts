import { Injectable } from '@angular/core';
import {UrlFactoryService} from "../../factories/url-factory.service";
import {RequestService} from "../request.service";
import {Ticket} from "../../../shared/models/ticket/ticket";

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private urlFactoryService: UrlFactoryService,
              private requestService: RequestService) { }

  async saveTicket(title: string, content: string){
    const url = this.urlFactoryService.getTicketUrl();
    return await this.requestService.save<any, Ticket>(url, {title, content, characterId: localStorage.getItem("characterId")});
  }

  async getCharacterTickets(): Promise<Ticket[]> {
    const url = this.urlFactoryService.getTicketUrl() + '/character/' + localStorage.getItem("characterId");
    return await this.requestService.get<Ticket[]>(url);
  }
}
