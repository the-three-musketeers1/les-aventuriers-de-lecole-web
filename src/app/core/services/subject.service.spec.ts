import { TestBed } from '@angular/core/testing';

import { SubjectService } from './subject.service';
import {UrlFactoryService} from '../../core/factories/url-factory.service';
import {RequestService} from '../../core/services/request.service';

xdescribe('SubjectServiceService', () => {
  let service: SubjectService;
  let mockUrlFactoryService;
  let mockRequestService;

  beforeEach(() => {
    mockUrlFactoryService = jasmine.createSpyObj('UrlFactoryService', ['']);
    mockRequestService = jasmine.createSpyObj('RequestService', ['']);
    TestBed.configureTestingModule({
      declarations: [SubjectService],
      providers: [
        {
          provide: UrlFactoryService,
          useValue: mockUrlFactoryService
        },
        {
          provide: RequestService,
          useValue: RequestService
        }
      ]
    });
    service = TestBed.inject(SubjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
