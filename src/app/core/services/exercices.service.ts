import { Injectable } from '@angular/core';
import {UrlFactoryService} from "../../core/factories/url-factory.service";
import {RequestService} from "../../core/services/request.service";
import {Exercise} from "../../shared/models/exercise/exercise";
import {ExerciseWithCharacterData} from "../../shared/models/exercise/exercise-with-character-data";


@Injectable({
  providedIn: 'root'
})
export class ExercicesService {

  constructor(private urlFactory : UrlFactoryService,
              private requestService: RequestService) {

  }

  getExercicesOfExpeditions(id): Promise<Exercise[]> {
    const url = this.urlFactory.getExercisesByExpeditionsIdUrl(id)
    return this.requestService.get(url);
  }

  getExercicesOfExpeditionsWithCharacterData(expeditionId, characterId): Promise<ExerciseWithCharacterData[]> {
    const url = this.urlFactory.getExercisesByExpeditionsIdWithCharacterDataUrl(expeditionId, characterId);
    return this.requestService.get(url);
  }

  saveExerciseAnswer(answers) {
    const url = this.urlFactory.getSaveAnswersUrl();
    return this.requestService.save(url,answers);
  }
}
