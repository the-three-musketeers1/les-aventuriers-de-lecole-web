import {AuthService} from './auth.service';
import {User} from '../../../shared/models/user/user';

describe('AuthService', () => {
  let mockHttpClient;
  let mockUrlFactoryService;
  let mockRequestService;
  let mockRouter;
  let mockLocalStorage;
  let authService: AuthService;
  let spyLocalStorageSetItem;
  let spyLocalStorageGetItem;
  let spyLocalStorageRemoveItem;
  let store;

  beforeEach(() => {
    mockHttpClient = jasmine.createSpyObj('HttpClient', ['']);
    mockUrlFactoryService = jasmine.createSpyObj('UrlFactoryService', ['getAuthUrl']);
    mockRequestService = jasmine.createSpyObj('RequestService', ['add', 'delete']);
    mockRouter = jasmine.createSpyObj('Router', ['navigate']);

    store = {};
    mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      }
    };
    spyLocalStorageGetItem = spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);
    spyLocalStorageSetItem = spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);
    spyLocalStorageRemoveItem = spyOn(localStorage, 'removeItem')
      .and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, 'clear')
      .and.callFake(mockLocalStorage.clear);
    authService = new AuthService(mockHttpClient, mockUrlFactoryService, mockRequestService, mockRouter);
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  describe('method subscribe', () => {
    it('should call url factory to get subscribe url', async () => {
      const subscribe: User = {
        firstname: 'toto',
        lastname: 'tata'
      };
      await authService.subscribe(subscribe);

      expect(mockUrlFactoryService.getAuthUrl).toHaveBeenCalledTimes(1);
      expect(mockUrlFactoryService.getAuthUrl).toHaveBeenCalledWith('subscribe');
    });
    it('should send request with user data and is_teacher to false to add user', async () => {
      const subscribe: User = {
        firstname: 'toto',
        lastname: 'tata'
      };
      const sendData = {...subscribe, is_teacher: false};
      mockUrlFactoryService.getAuthUrl.and.returnValue('https://test.org/subcribe');
      await authService.subscribe(subscribe);

      expect(mockRequestService.add).toHaveBeenCalledTimes(1);
      expect(mockRequestService.add).toHaveBeenCalledWith('https://test.org/subcribe', sendData);
    });
  });

  describe('method login', () => {
    const userLogin: User = {
      email: 'toto@tata.fr',
      password: 'tototata'
    };
    const tokenToCheck = {hash: 'oijozeifjoerg', userId: '1'};
    beforeEach(() => {
      mockUrlFactoryService.getAuthUrl.and.returnValue('https://test.org/login');
      const token = JSON.stringify(tokenToCheck);
      mockRequestService.add.and.returnValue({token});
    });

    it('should send request login of user', async () => {
      await authService.login(userLogin);
      expect(mockUrlFactoryService.getAuthUrl).toHaveBeenCalledTimes(1);
      expect(mockUrlFactoryService.getAuthUrl).toHaveBeenCalledWith('login');
    });

    it('should set 2 items in local storage', async () => {
      await authService.login(userLogin);
      expect(spyLocalStorageSetItem).toHaveBeenCalledTimes(2);
    });

    it('should set item of hash in local storage', async () => {
      await authService.login(userLogin);
      expect(spyLocalStorageSetItem.calls.all()[0].args).toEqual(['hash', tokenToCheck.hash]);
    });

    it('should set item of user id in local storage', async () => {
      await authService.login(userLogin);
      expect(spyLocalStorageSetItem.calls.all()[1].args).toEqual(['uid', tokenToCheck.userId]);
    });
  });

  describe('method getToken', () => {
    beforeEach(() => {
      store = {
        hash: '4ze98f4z65ef4z',
        uid: '1'
      };
    });

    it('should return undefined when localStorage don\'t have hash item', () => {
      store.hash = null;
      const checkResult = authService.getToken();
      expect(checkResult).toBeUndefined();
    });

    it('should return undefined when localStorage don\'t have hash uid', () => {
      store.uid = null;
      const checkResult = authService.getToken();
      expect(checkResult).toBeUndefined();
    });

    it('should return token when localeStorage have hash and uid', () => {
      const checkResult = authService.getToken();
      expect(checkResult).toBe(JSON.stringify({
        hash: store.hash,
        userId: +store.uid
      }));
    });
  });

  describe('getter isLoggedIn', () => {
    beforeEach(() => {
      store = {
        hash: '4ze98f4z65ef4z',
        uid: '1'
      };
    });

    it('should return false when localStorage don\'t have hash item', () => {
      store.hash = null;
      expect(authService.isLoggedIn).toBe(false);
    });
    it('should return false when localStorage don\'t have uid item', () => {
      store.uid = null;
      expect(authService.isLoggedIn).toBe(false);
    });
    it('should return true when localStorage have hash and uid items', () => {
      expect(authService.isLoggedIn).toBe(true);
    });
  });

  describe('method logout', () => {
    beforeEach(() => {
      store = {
        hash: '4ze98f4z65ef4z',
        uid: '1'
      };
    });

    it('should call uid item of localStorage', async () => {
      await authService.logout();
      expect(spyLocalStorageGetItem).toHaveBeenCalledTimes(1);
      expect(spyLocalStorageGetItem).toHaveBeenCalledWith('uid');
    });
    it('should call getAuthUrl of urlFactoryService', async () => {
      await authService.logout();
      expect(mockUrlFactoryService.getAuthUrl).toHaveBeenCalledTimes(1);
      expect(mockUrlFactoryService.getAuthUrl).toHaveBeenCalledWith('logout');
    });
    it('should call delete of requestService', async () => {
      const logoutUrl = 'https://test/logout';
      const uid = store.uid;
      mockUrlFactoryService.getAuthUrl.and.returnValue(logoutUrl);
      await authService.logout();
      expect(mockRequestService.delete).toHaveBeenCalledTimes(1);
      expect(mockRequestService.delete).toHaveBeenCalledWith(`${logoutUrl}`);
    });
    it('should console error if requestService fail', async () => {
      spyOn(console, 'error');
      mockRequestService.delete.and.returnValue(Promise.reject(new Error('fail')));

      await authService.logout();
      expect(console.error).toHaveBeenCalledTimes(1);
      expect(console.error).toHaveBeenCalledWith(new Error('fail'));
    });
    it('should remove hash and uid item', async () => {
      await authService.logout();
      expect(spyLocalStorageRemoveItem).toHaveBeenCalledTimes(2);
      expect(spyLocalStorageRemoveItem.calls.all()[0].args).toEqual(['hash']);
      expect(spyLocalStorageRemoveItem.calls.all()[1].args).toEqual(['uid']);
    });
    it('should route to home path', async () => {
      await authService.logout();
      expect(mockRouter.navigate).toHaveBeenCalledTimes(1);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['home']);
    });
  });
});
