import {Injectable} from '@angular/core';
import {UrlFactoryService} from '../../factories/url-factory.service';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../../../shared/models/user/user';
import {RequestService} from '../request.service';
import {Session} from '../../../shared/models/session/session';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private urlFactoryService: UrlFactoryService,
    private requestService: RequestService,
    public router: Router
  ) {
  }

  /**
   * request to subscribe user
   */
  async subscribe(user: User): Promise<User> {
    const sendData = {...user, is_teacher: false};
    return await this.requestService.add<User>(this.urlFactoryService.getAuthUrl('subscribe'), sendData);
  }

  /**
   * request to login user
   */
  async login(user: User): Promise<void> {
    const session = await this.requestService.add<Session>(this.urlFactoryService.getAuthUrl('login'), user);
    const token = JSON.parse(session.token);

    localStorage.setItem('hash', token.hash);
    localStorage.setItem('uid', token.userId);

    await this.router.navigate(['/']);
  }

  /**
   * get auth token of login user
   */
  getToken(): string | undefined {
    if (localStorage.getItem('hash') && localStorage.getItem('uid')) {
      return JSON.stringify({
        hash: localStorage.getItem('hash'),
        userId: +localStorage.getItem('uid')
      });
    }
  }

  /**
   * check if user is login
   */
  get isLoggedIn(): boolean {
    const authToken = localStorage.getItem('hash');
    const userId = localStorage.getItem('uid');
    return (authToken !== null && userId !== null);
  }

  /**
   * request to logout user
   */
  async logout(): Promise<void> {
    try {
      const userId = localStorage.getItem('uid');
      const logoutUrl = this.urlFactoryService.getAuthUrl('logout');

      await this.requestService.delete(`${logoutUrl}`);
    } catch (e) {
      console.error(e);
    } finally {
      localStorage.removeItem('hash');
      localStorage.removeItem('uid');
      await this.router.navigate(['home']);
    }
  }
}
