import { Injectable } from '@angular/core';
import {UrlFactoryService} from "../../factories/url-factory.service";
import {RequestService} from "../request.service";
import {Message} from "../../../shared/models/message/message";

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private urlFactoryService: UrlFactoryService,
              private requestService: RequestService) { }

  async saveMessage(content: string, ticketId: number){
    const url = this.urlFactoryService.getMessageUrl() + '/' + ticketId + '/character';
    return await this.requestService.save<any, Message>(url, {content, characterId: localStorage.getItem("characterId")});
  }

  async getTicketMessages(ticketId: number): Promise<Message[]> {
    const url = this.urlFactoryService.getTicketUrl() + '/' + ticketId + '/messages/character/' + localStorage.getItem("characterId");
    return await this.requestService.get<Message[]>(url);
  }
}
