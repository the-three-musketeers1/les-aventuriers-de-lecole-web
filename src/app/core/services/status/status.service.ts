import { Injectable } from '@angular/core';
import {UrlFactoryService} from "../../factories/url-factory.service";
import {RequestService} from "../request.service";
import {Status} from "../../../shared/models/status/status";

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  constructor(private urlFactoryService: UrlFactoryService,
              private requestService: RequestService) { }

  async getStatuses(): Promise<Status[]> {
    const url = this.urlFactoryService.getStatusUrl();
    return await this.requestService.get<Status[]>(url);
  }
}
