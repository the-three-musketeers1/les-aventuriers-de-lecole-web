import { Injectable } from '@angular/core';
import {UrlFactoryService} from '../../core/factories/url-factory.service';
import {RequestService} from '../../core/services/request.service';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  constructor(private urlFactory: UrlFactoryService,
              private requestService: RequestService) { }


  getDisciplineByCharacterId(id: number): Promise<any> {
    const url = this.urlFactory.getDisciplineByCharacterIdUrl(id);
    return this.requestService.getAll(url);
  }

  getChapterByDisciplineId(id: number) {
    const url = this.urlFactory.getChapterByDisciplineIdUrl(id);
    return this.requestService.getAll(url);
  }
}
