import { Injectable } from '@angular/core';
import {UrlFactoryService} from "../../core/factories/url-factory.service";
import {RequestService} from "../../core/services/request.service";
import {Exercise} from "../../shared/models/exercise/exercise";


@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor(private urlFactory : UrlFactoryService,
              private requestService: RequestService) { }

  getAllQuestionByExercise(id): Promise<Exercise> {
    const characterId = localStorage.getItem("characterId");
    const url = this.urlFactory.getQuestionsByExerciseUrl(id, characterId);
    return this.requestService.get(url);
  }

}
