import { Injectable } from '@angular/core';
import {UrlFactoryService} from "../../factories/url-factory.service";
import {RequestService} from "../request.service";
import {ExpeditionInformation} from "../../../shared/models/expedition-information/expedition-information";

@Injectable({
  providedIn: 'root'
})
export class ExpeditionService {

  constructor(
    private urlFactoryService: UrlFactoryService,
    private requestService: RequestService,
  ) { }

  getAvailableExpeditionsByCharacter() :Promise<ExpeditionInformation[]> {
    const characterId = localStorage.getItem("characterId");
    const url = this.urlFactoryService.getAvailableExpeditionUrl(characterId);
    return this.requestService.getAll(url);
  }

  getAllExpeditionsByCharacter(): Promise<ExpeditionInformation[]> {
    const characterId = localStorage.getItem("characterId");
    const url = this.urlFactoryService.getAllExpeditionUrl(characterId);
    return this.requestService.getAll(url);
  }

  checkEndOfExpedition(expeditionId: number, charId: number): Promise<any> {
    const url = this.urlFactoryService.getEndOfExpeditionUrl();
    const data = {
      expeditionId: expeditionId,
      characterId: charId
    }
    return this.requestService.save(url,data);
  }

  getExpeditionById(expeditionId: number): Promise<any> {
    const url = this.urlFactoryService.getExpeditionByIdUrl(expeditionId);
    return this.requestService.get(url);
  }
}
