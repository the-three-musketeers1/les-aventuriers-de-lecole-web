import { TestBed } from '@angular/core/testing';

import { RolePictureService } from './role-picture.service';

describe('RolePictureService', () => {
  let service: RolePictureService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RolePictureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('method getRolePicturePathByRoleName', () => {
    it('should return warrior image path when Guerrier name is pass in parameter', () => {
      const warriorPicture = service.getRolePicturePathByRoleName('Guerrier');
      expect(warriorPicture).toBe('assets/images/warrior-portrait.png');
    });

    it('should return healer image path when Soigneur name is pass in parameter', () => {
      const healerPicture = service.getRolePicturePathByRoleName('Soigneur');
      expect(healerPicture).toBe('assets/images/healer-portrait.png');
    });
    it('should return witch image path when Mage role is pass in parameter', () => {
      const witchPicture = service.getRolePicturePathByRoleName('Mage');
      expect(witchPicture).toBe('assets/images/witch-portrait.png');
    });
    it('should return thief image path when Voleur role is pass in parameter', () => {
      const thiefPicture = service.getRolePicturePathByRoleName('Voleur');
      expect(thiefPicture).toBe('assets/images/thief-portrait.png');
    });
    it('should return unknown image path when role name not correspond to given role image paths', () => {
      const unknownRolePicture = service.getRolePicturePathByRoleName('Unknown');
      expect(unknownRolePicture).toBe('assets/images/unknown-portrait.png');
    });
  });
});
