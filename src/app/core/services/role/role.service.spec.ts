import { TestBed } from '@angular/core/testing';

import { RoleService } from './role.service';
import {UrlFactoryService} from '../../factories/url-factory.service';
import {RequestService} from '../request.service';

describe('RoleService', () => {
  let service: RoleService;
  let mockUrlFactoryService;
  let mockRequestService;

  beforeEach(() => {
    mockUrlFactoryService = jasmine.createSpyObj('UrlFactoryService', ['getRoleUrl']);
    mockRequestService = jasmine.createSpyObj('RequestService', ['getAll']);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: UrlFactoryService,
          useValue: mockUrlFactoryService
        },
        {
          provide: RequestService,
          useValue: mockRequestService
        }
      ]
    });
    service = TestBed.inject(RoleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('method getRoles', () => {
    it('should call urlFactoryService to get role url', () => {
      service.getRoles();
      expect(mockUrlFactoryService.getRoleUrl).toHaveBeenCalledTimes(1);
    });
    it('should call request service get all Role', () => {
      mockUrlFactoryService.getRoleUrl.and.returnValue('url-role');
      service.getRoles();
      expect(mockRequestService.getAll).toHaveBeenCalledTimes(1);
      expect(mockRequestService.getAll).toHaveBeenCalledWith('url-role');
    });
  });
});
