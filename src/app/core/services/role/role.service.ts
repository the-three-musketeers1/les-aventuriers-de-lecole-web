import {Injectable} from '@angular/core';
import {UrlFactoryService} from '../../factories/url-factory.service';
import {RequestService} from '../request.service';
import {Role} from '../../../shared/models/role/role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(
    private urlFactoryService: UrlFactoryService,
    private requestService: RequestService
  ) {
  }

  getRoles(): Promise<Role[]> {
    return this.requestService.getAll<Role>(
      this.urlFactoryService.getRoleUrl()
    );
  }
}
