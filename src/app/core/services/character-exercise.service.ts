import { Injectable } from '@angular/core';
import {UrlFactoryService} from "../../core/factories/url-factory.service";
import {RequestService} from "../../core/services/request.service";
import {StatisticsExercise} from "../../shared/models/statistics-exercise";

@Injectable({
  providedIn: 'root'
})
export class CharacterExerciseService {

  constructor(private urlFactory : UrlFactoryService,
              private requestService: RequestService) {

  }

  getStatisticsExercises(characterId,disciplineId,chapterId,beginDate,endDate) : Promise<StatisticsExercise[]> {
    const url = this.urlFactory.getStatisticsExerciseUrl(characterId, disciplineId, chapterId, beginDate, endDate);
    return this.requestService.getAll(url);
  }
}
