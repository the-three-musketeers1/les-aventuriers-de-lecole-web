import {Injectable} from '@angular/core';
import {UrlFactoryService} from '../../factories/url-factory.service';
import {RequestService} from '../request.service';
import {CharacterInfoDto} from '../../../shared/models/character-info-dto';
import {Character} from '../../../shared/models/character/character';
import {Router} from '@angular/router';
import {Role} from '../../../shared/models/role/role';
import {User} from "../../../shared/models/user/user";

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(
    private urlFactoryService: UrlFactoryService,
    private requestService: RequestService,
    private router: Router
  ) {
  }

  async getCharacterStudent(id: number): Promise<Character> {
    const url = this.urlFactoryService.getCharacterUrl() + '/' + id + '/student';
    return await this.requestService.get(url);
  }

  getCharacterInformations(id): Promise<CharacterInfoDto> {
    const url = this.urlFactoryService.getCharacterInformationsUrl(id);
    return this.requestService.get(url);

  }

  addExperienceToCharacter(id: number, experience: number): Promise<any> {
    const url = this.urlFactoryService.getAddExperienceToCharacterUrl(id);
    return this.requestService.update(url, {experience: experience});
  }

  async getCharacterLeader(id: number): Promise<User> {
    const url = this.urlFactoryService.getCharacterUrl() + '/' + id + '/leader';
    return await this.requestService.get(url);
  }

  async createCharacter(guildId: number, name: string, role: Role): Promise<void> {
    const url = this.urlFactoryService.getGuildUrl() + `/${guildId}/characters`;
    const character = await this.requestService.save<any, Character>(url, {name, roleId: role.id});
    localStorage.setItem('characterId', character.id.toString());
    await this.router.navigate(['resume']);
  }

  async getCharacters(): Promise<Character[]> {
    const url = this.urlFactoryService.getCharacterUrl() + '/student';
    return this.requestService.getAll<Character>(url);
  }

  getIdInLocalStorage() {
    return localStorage.getItem('characterId');
  }

  removeIdInLocalStorage() {
    return localStorage.removeItem('characterId');
  }

  setIdInLocalStorage(characterId: number) {
    return localStorage.setItem('characterId', characterId.toString());
  }
}
