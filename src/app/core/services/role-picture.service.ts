import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RolePictureService {

  constructor() { }

  getRolePicturePathByRoleName(roleName: string): string {
    const rolePathsDict = {
      Guerrier: 'assets/images/warrior-portrait.png',
      Soigneur: 'assets/images/healer-portrait.png',
      Mage: 'assets/images/witch-portrait.png',
      Voleur: 'assets/images/thief-portrait.png'
    };
    return rolePathsDict[roleName] === undefined
      ? 'assets/images/unknown-portrait.png'
      : rolePathsDict[roleName];
  }
}
