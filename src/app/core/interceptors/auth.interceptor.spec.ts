import {inject, TestBed} from '@angular/core/testing';
import {AuthInterceptor} from './auth.interceptor';
import {AuthService} from '../services/auth/auth.service';

describe('AuthInterceptor', () => {
  let authInterceptor;
  let mockAuthService;
  let mockHttpRequest;
  let mockHttpHandler;

  beforeEach(() => {
      mockAuthService = jasmine.createSpyObj('AuthService', ['getToken']);
      TestBed.configureTestingModule(
        {
          providers: [
            AuthInterceptor,
            {
              provide: AuthService,
              useValue: mockAuthService
            }
          ]
        });
    }
  );

  beforeEach(inject([AuthInterceptor], (ai: AuthInterceptor) => {
    authInterceptor = ai;
  }));

  it('should be created', () => {
    const interceptor: AuthInterceptor = TestBed.inject(AuthInterceptor);
    expect(interceptor).toBeTruthy();
  });

  describe('method intercept', () => {
    beforeEach(() => {
      mockHttpRequest = jasmine.createSpyObj('HttpClient', ['clone']);
      mockHttpHandler = jasmine.createSpyObj('HttpHandler', ['handle']);
    });
    it('should call auth service to get token', () => {
      authInterceptor.intercept(mockHttpRequest, mockHttpHandler);
      expect(mockAuthService.getToken).toHaveBeenCalledTimes(1);
    });
    it('should update request header with authorization property', () => {
      const tokenLogin = '6a9ze4f6a5z4ef654azef';
      mockAuthService.getToken.and.returnValue(tokenLogin);

      authInterceptor.intercept(mockHttpRequest, mockHttpHandler);

      expect(mockHttpRequest.clone).toHaveBeenCalledTimes(1);
      expect(mockHttpRequest.clone).toHaveBeenCalledWith({
        setHeaders: {
          Authorization: `Bearer ${tokenLogin}`
        }
      });
    });
    it('should handler the next intercept', () => {
      mockHttpRequest.clone.and.returnValue(mockHttpRequest);

      authInterceptor.intercept(mockHttpRequest, mockHttpHandler);

      expect(mockHttpHandler.handle).toHaveBeenCalledTimes(1);
      expect(mockHttpHandler.handle).toHaveBeenCalledWith(mockHttpRequest);
    });
    it('should return Observable of HttpEvent', () => {
      const httpEvent$ = jasmine.createSpyObj('Observable<HttpEvent<any>>', ['']);
      mockHttpHandler.handle.and.returnValue(httpEvent$);

      const result = authInterceptor.intercept(mockHttpRequest, mockHttpHandler);

      expect(result).toEqual(httpEvent$);
    });
  });
});
