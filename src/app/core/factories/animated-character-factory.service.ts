import { Injectable } from '@angular/core';
import {AnimatedCharacter} from "../../shared/models/animated-character"
@Injectable({
  providedIn: 'root'
})
export class AnimatedCharacterFactoryService {

  constructor() { }

  private listMonsterName = ["Glacior","Petror","Lavaor","Uranior","Spacior","Desertior"];

  getWarriorAnimatedCharacter() {
    const warrior = new AnimatedCharacter();
    warrior.filePath = "assets/images/chevalier.png";
    warrior.currentPosition = null;
    warrior.attackPosition = {x:0,y:425,sw:95,sh:50,dx:100,dy:50,dw:200,dh:150};
    warrior.takeDmgPosition = {x:22,y:230,sw:67,sh:57,dx:125,dy:50,dw:150,dh:150};
    warrior.waitingPosition = {x:22,y:36,sw:66,sh:57,dx:125,dy:50,dw:150,dh:150};
    return warrior;
  }

  getHealerAnimatedCharacter() {
    const healer = new AnimatedCharacter();
    healer.filePath = "assets/images/soigneur.png";
    healer.currentPosition = null;
    healer.takeDmgPosition = {x:22,y:230,sw:67,sh:57,dx:125,dy:50,dw:150,dh:150};
    healer.attackPosition = {x:0,y:425,sw:95,sh:50,dx:100,dy:50,dw:200,dh:150};
    healer.waitingPosition = {x:22,y:36,sw:66,sh:57,dx:125,dy:50,dw:150,dh:150};
    return healer;
  }

  getThiefAnimatedCharacter() {
    const thief = new AnimatedCharacter();
    thief.filePath = "assets/images/voleur.png";
    thief.currentPosition = null;
    thief.attackPosition = {x:0,y:425,sw:95,sh:50,dx:100,dy:50,dw:200,dh:150};
    thief.takeDmgPosition = {x:22,y:230,sw:67,sh:57,dx:125,dy:50,dw:150,dh:150};
    thief.waitingPosition = {x:22,y:36,sw:66,sh:57,dx:125,dy:50,dw:150,dh:150};
    return thief;
  }

  getMageAnimatedCharacter() {
    const mage = new AnimatedCharacter();
    mage.filePath = "assets/images/mage.png";
    mage.currentPosition = null;
    mage.takeDmgPosition = {x:15,y:230,sw:67,sh:57,dx:125,dy:50,dw:150,dh:150};
    mage.attackPosition = {x:-2,y:582,sw:95,sh:80,dx:100,dy:15,dw:200,dh:185};
    mage.waitingPosition = {x:22,y:133,sw:66,sh:57,dx:125,dy:50,dw:150,dh:150};
    return mage;
  }


  getMonsterAnimatedCharacter() {
    const monster = new AnimatedCharacter();
    const nb = Math.floor(Math.random() * Math.floor(6));
    monster.filePath = "assets/images/monster" + (nb + 1)  + ".png";
    monster.name = this.listMonsterName[nb];
    monster.currentPosition = null;
    monster.takeDmgPosition = {x:0,y:427,sw:190,sh:95,dx:70,dy:70,dw:200,dh:135};
    monster.attackPosition = {x:0,y:539,sw:190,sh:105,dx:53,dy:70,dw:200,dh:135};

    monster.waitingPosition = {x:0,y:35,sw:190,sh:95,dx:73,dy:70,dw:200,dh:135};
    return monster;
  }
}
