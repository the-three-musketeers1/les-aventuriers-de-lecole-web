import { TestBed } from '@angular/core/testing';

import { DonutChartFactoryService } from './donut-chart-factory.service';

describe('DonutChartFactoryService', () => {
  let service: DonutChartFactoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DonutChartFactoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
