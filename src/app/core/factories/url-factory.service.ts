import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UrlFactoryService {

  constructor() {
  }

  getQuestionsByExerciseUrl(id, characterId) {
    return environment.apiUrl + '/exercise/' + id + '/questions' + '/character/' + characterId;
  }

  getExercisesByExpeditionsIdUrl(id) {
    return environment.apiUrl + '/exercise/expedition/' + id;
  }

  getExercisesByExpeditionsIdWithCharacterDataUrl(expeditionId, characterId) {
    return environment.apiUrl + '/exercise/expedition/' + expeditionId + '/character/' + characterId;

  }

  getSaveAnswersUrl() {
    return environment.apiUrl + '/characterQuestion';
  }

  getQuestionsUrl() {
    return environment.apiUrl + '/questions';
  }

  getAuthUrl(complement: string) {
    return `${environment.apiUrl}/auth/${complement}`;
  }

  getStatisticsExerciseUrl(characterId: number, disciplineId: number, chapterId: number, beginDate: any, endDate: any) {
    return environment.apiUrl + '/characterExercise/character/' + characterId
      + '?disciplineId=' + disciplineId
      + '&chapterId=' + chapterId
      + '&beginDate=' + beginDate
      + '&endDate=' + endDate;
  }

  getDisciplineByCharacterIdUrl(id) {
    return environment.apiUrl + '/subjects/character/' + id;
  }

  getCharacterUrl() {
    return environment.apiUrl + '/character';
  }

  getChapterByDisciplineIdUrl(id: number) {
    return environment.apiUrl + '/subjects/disciplineChapter/' + id;
  }

  getCharacterInformationsUrl(id: any) {
    return environment.apiUrl + '/character/' + id;
  }

  getAddExperienceToCharacterUrl(id: number) {
    return environment.apiUrl + '/character/' + id + '/experience';
  }

  getGuildUrl() {
    return environment.apiUrl + '/guild';
  }

  getRoleUrl() {
    return environment.apiUrl + '/roles';
  }

  getAvailableExpeditionUrl(characterId: string) {
    return environment.apiUrl + '/adventures/expeditions/available/character/' + characterId
  }

  getAllExpeditionUrl(characterId: string) {
    return environment.apiUrl + '/adventures/expeditions/character/' + characterId
  }

  getEndOfExpeditionUrl() {
    return environment.apiUrl + '/characterAdventure/ending/'
  }

  getTicketUrl() {
      return environment.apiUrl + '/ticket';
  }

  getStatusUrl() {
    return environment.apiUrl + '/status';
  }

  getMessageUrl() {
    return environment.apiUrl + '/message';
  }

  getExpeditionByIdUrl(expeditionId: number) {
    return environment.apiUrl + '/adventures/expeditions/' + expeditionId
  }
}
