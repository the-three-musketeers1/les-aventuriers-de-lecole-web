import { Injectable } from '@angular/core';
import {StatisticsExercise} from "../../shared/models/statistics-exercise";

@Injectable({
  providedIn: 'root'
})
export class DonutChartFactoryService {

  constructor() { }

  getAnswersDataFromStatisticsExercise(stats: StatisticsExercise[]) {
    const goodAnswers = stats.reduce((acc,val) => {
      acc += val.goodAnswers;
      return acc;
    },0);
    const badAnswers = stats.reduce((acc,val) => {
      acc += val.badAnswers;
      return acc;
    },0);

    return {
      labels: ['Bonne réponse','Mauvaise Réponse'],
      datasets: [
        {
          data: [goodAnswers,badAnswers],
          backgroundColor: [
            "#44d238",
            "#d94b58"
          ],
          hoverBackgroundColor: [
            "#32cd23",
            "#cb2230"
          ]
        }]
    };

  }

  getMonstersDataFromStatisticsExercise(stats: StatisticsExercise[]) {
    const killedMonster = stats.filter(monster => monster.damage >= monster.exerciseLife).length;
    const survivingMonster = stats.filter(monster => monster.damage < monster.exerciseLife).length;

    return {
      labels: ['Monstre Vaincu','Monstre survivant'],
      datasets: [
        {
          data: [killedMonster,survivingMonster],
          backgroundColor: [
            "#44d238",
            "#d94b58"
          ],
          hoverBackgroundColor: [
            "#32cd23",
            "#cb2230"
          ]
        }]
    };
  }
}
