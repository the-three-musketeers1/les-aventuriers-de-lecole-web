import { TestBed } from '@angular/core/testing';

import { UrlFactoryService } from './url-factory.service';

describe('UrlFactoryService', () => {
  let service: UrlFactoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UrlFactoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
