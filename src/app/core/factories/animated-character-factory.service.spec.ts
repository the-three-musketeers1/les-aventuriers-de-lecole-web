import { TestBed } from '@angular/core/testing';

import { AnimatedCharacterFactoryService } from './animated-character-factory.service';

describe('AnimatedCharacterFactoryService', () => {
  let service: AnimatedCharacterFactoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnimatedCharacterFactoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
