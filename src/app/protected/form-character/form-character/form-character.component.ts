import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {Role} from '../../../shared/models/role/role';
import {CharacterService} from '../../../core/services/character/character.service';
import {RoleService} from '../../../core/services/role/role.service';
import {RolePictureService} from "../../../core/services/role-picture.service";

@Component({
  selector: 'app-form-character',
  templateUrl: './form-character.component.html',
  styleUrls: ['./form-character.component.scss']
})
export class FormCharacterComponent implements OnInit {

  isProcess = false;
  guildId: number;
  roles: Role[];
  selectedRole: Role;
  rolePicturePath: string;
  roleControl: FormControl;
  nameControl: FormControl;
  characterForm: FormGroup;
  translateRoles = {
    warrior: 'guerrier',
    priest: 'soigneur',
    wizard: 'sorcier',
    thief: 'voleur'
  };
  displayModal = false;
  isSuccess = false;
  statusCode = 0;
  customErrors = {};
  failNext = undefined;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private roleService: RoleService,
    private rolePictureService: RolePictureService,
    private characterService: CharacterService
  ) {
    roleService.getRoles().then(roles => {
      this.roles = roles;
      if (this.roleControl) {
        this.roleControl.setValue(this.roles[0]);
      }
      this.roles.forEach(role => {
        if (this.translateRoles[role.name] === undefined) {
          this.translateRoles[role.name] = role.name;
        }
      });
    });
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.guildId = params.guildId;
    });

    try {
      this.nameControl = this.formBuilder.control('', [Validators.required]);
      this.roleControl = this.formBuilder.control(this.roles);
      this.roleControl.valueChanges.subscribe(role => {
        this.selectedRole = role;
        this.rolePicturePath = this.rolePictureService.getRolePicturePathByRoleName(this.selectedRole.name);
      });
      this.characterForm = this.formBuilder.group({
        name: this.nameControl,
        role: this.roleControl
      });
    } catch (e) {
      console.log(e);
    }
  }

  async onSubmit() {
    const name = this.nameControl.value;
    const role = this.roleControl.value;
    try {
      this.displayModal = false;
      if (!this.isProcess) {
        await this.characterService.createCharacter(this.guildId, name, role);
        this.isProcess = false;
      }
      this.isProcess = true;
    } catch (err) {
      this.statusCode = err.status;
      this.isSuccess = false;
    } finally {
      this.displayModal = true;
    }
  }
}
