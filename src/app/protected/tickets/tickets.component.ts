import {Component, OnInit, ViewChild} from '@angular/core';
import {TicketService} from "../../core/services/ticket/ticket.service";
import {Ticket} from "../../shared/models/ticket/ticket";
import {StatusService} from "../../core/services/status/status.service";
import {Status} from "../../shared/models/status/status";
import {Table} from "primeng/table";
import {Router} from "@angular/router";
import {CharacterService} from "../../core/services/character/character.service";
import {CharacterInfoDto} from "../../shared/models/character-info-dto";

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit {

  tickets: Ticket[] = [];
  statuses: Status[] = [];
  displayStatuses: any[] = [];
  character: CharacterInfoDto;

  @ViewChild('dt') table: Table;
  displayStatusesLoaded: boolean = false;

  constructor(private ticketService: TicketService,
              private statusService: StatusService,
              private characterService: CharacterService,
              private router: Router) { }

  ngOnInit(): void {
    const charId = localStorage.getItem("characterId");
    if (charId !== null) {
      this.characterService.getCharacterInformations(parseInt(charId)).then(result => {
        this.character = result;
      })
        .catch(console.error);
    }
    this.ticketService.getCharacterTickets().then(res => {
      this.tickets = res;
      this.statusService.getStatuses().then(res => {
        this.statuses = res;
        for (let status of this.statuses) {
          this.displayStatuses.push({label : status.state, value: status.id});
        }
        for (let ticket of this.tickets) {
          ticket.displayStatus = {label: ticket.status.state, value: ticket.status.id};
        }
        this.displayStatusesLoaded = true;
      });
    });
  }

  onSelectTicket(ticketId: number) {
    this.router.navigate(['/messages', ticketId]);
  }

  onDateSelect(value) {
    this.table.filter(this.formatDate(value), 'date', 'equals');
  }

  formatDate(date) {

    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (month < 10) {
      month = '0' + month;
    }

    if (day < 10) {
      day = '0' + day;
    }

    return date.getFullYear() + '-' + month + '-' + day;
  }
}
