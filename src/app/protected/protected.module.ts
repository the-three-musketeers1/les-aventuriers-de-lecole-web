import {NgModule} from '@angular/core';
import {SharedModule} from 'src/app/shared/shared.module';
import {ProtectedRoutingModule} from './protected-routing.module';
import {ResumeComponent} from './connected-home/resume/resume.component';
import {ProgressBarModule} from 'primeng/progressbar';
import {ToastModule} from 'primeng/toast';
import {ConnectedHomeModule} from './connected-home/connected-home.module';
import {ExpeditionExercisesModule} from './expedition-exercises/expedition-exercises.module';
import {ExercisesModule} from './exercises/exercises.module';
import {ExerciseComponent} from './exercises/exercise/exercise.component';
import {StatisticsModule} from './statistics/statistics.module';
import {ListCharactersComponent} from './list-characters/list-characters/list-characters.component';
import {CheckingGuildComponent} from './checking-guild/checking-guild/checking-guild.component';
import {InputTextModule} from 'primeng/inputtext';
import {ReactiveFormsModule} from '@angular/forms';
import {FormCharacterComponent} from './form-character/form-character/form-character.component';
import {TableModule} from 'primeng/table';
import {ExpeditionsComponent} from './expeditions/expeditions.component';
import {TicketsComponent} from './tickets/tickets.component';
import {CalendarModule} from 'primeng/calendar';
import {MultiSelectModule} from 'primeng/multiselect';
import {DropdownModule} from 'primeng/dropdown';
import {MessagesComponent} from './messages/messages.component';
import {MessageDialogComponent} from './message-dialog/message-dialog.component';
import {DialogModule} from 'primeng/dialog';
import {TooltipModule} from 'primeng/tooltip';

@NgModule({
  declarations: [
    ResumeComponent,
    ExerciseComponent,
    ListCharactersComponent,
    CheckingGuildComponent,
    FormCharacterComponent,
    ExpeditionsComponent,
    TicketsComponent,
    MessagesComponent,
    MessageDialogComponent
  ],
  imports: [
    SharedModule,
    InputTextModule,
    ProtectedRoutingModule,
    ProgressBarModule,
    ToastModule,
    ConnectedHomeModule,
    ExpeditionExercisesModule,
    ExercisesModule,
    StatisticsModule,
    ReactiveFormsModule,
    TableModule,
    CalendarModule,
    MultiSelectModule,
    DropdownModule,
    DialogModule,
    TooltipModule
  ],
  exports: [ResumeComponent]
})
export class ProtectedModule {
}
