import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GuildService} from '../../../core/services/guild/guild.service';

@Component({
  selector: 'app-checking-gulid',
  templateUrl: './checking-guild.component.html',
  styleUrls: ['./checking-guild.component.scss']
})
export class CheckingGuildComponent implements OnInit {

  errors: string[] = [];
  checkingGuildForm: FormGroup;
  guildName: FormControl;
  guildToken: FormControl;

  constructor(
    private formBuilder: FormBuilder,
    private guildService: GuildService
  ) {
  }

  ngOnInit(): void {
    this.guildName = this.formBuilder.control('', [Validators.required]);
    this.guildToken = this.formBuilder.control('', [Validators.required]);

    this.checkingGuildForm = this.formBuilder.group({
      guildName: this.guildName,
      guildToken: this.guildToken
    });
  }

  async onCheckGuild() {
    const guildName = this.guildName.value as string;
    const guildToken = this.guildToken.value as string;
    try {
      await this.guildService.getGuildByNameAndToken(guildName, guildToken);
    } catch (e) {
      console.error(e);
      this.errors[0] = e.error.error;
    }
  }
}
