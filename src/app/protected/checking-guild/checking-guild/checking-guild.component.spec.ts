import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckingGuildComponent } from './checking-guild.component';

describe('CheckingGulidComponent', () => {
  let component: CheckingGuildComponent;
  let fixture: ComponentFixture<CheckingGuildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckingGuildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckingGuildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
