import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {MessageService} from "../../core/services/message/message.service";
import {Message} from "../../shared/models/message/message";
import {CharacterService} from "../../core/services/character/character.service";
import {CharacterInfoDto} from "../../shared/models/character-info-dto";

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  messages: Message[] = [];
  displayStatuses: any[] = [];
  displayMessageDialog: boolean = false;
  ticketId: number;
  character: CharacterInfoDto;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private messageService: MessageService,
              private characterService: CharacterService) { }

  ngOnInit(): void {
    const charId = localStorage.getItem("characterId");
    if (charId !== null) {

      this.characterService.getCharacterInformations(parseInt(charId)).then(result => {
        this.character = result;
      })
        .catch(console.error);
    }
    this.ticketId = +this.route.snapshot.paramMap.get('ticketId');

    this.displayStatuses.push({label: 'Envoyé', value: 1});
    this.displayStatuses.push({label: 'Reçu', value: 0});
    this.getTicketMessages();
  }

  getTicketMessages(): void {
    this.messageService.getTicketMessages(this.ticketId).then(res => {
      this.messages = res;
      this.messages.forEach(message => {
        message.state = {
          label: message.sentByCharacter ? 'Envoyé' : 'Reçu',
          value: message.sentByCharacter ? 1 : 0
        }
      });
    });
  }

  showMessageDialog() {
    this.displayMessageDialog = true;
  }

  hideMessageDialog() {
    this.displayMessageDialog = false;
    this.getTicketMessages();
  }

  getTicketId() {
    return this.ticketId;
  }
}
