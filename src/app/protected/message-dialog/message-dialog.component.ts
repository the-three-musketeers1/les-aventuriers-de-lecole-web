import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {CharacterService} from "../../core/services/character/character.service";
import {MessageService} from "../../core/services/message/message.service";

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.scss']
})
export class MessageDialogComponent implements OnInit {

  @Input() display: boolean;
  @Input() ticketId: number;
  @Output() closeDialog = new EventEmitter();

  messageForm: FormGroup;
  sendSucceed: boolean = false;
  sendFailed: boolean = false;

  constructor(private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private characterService: CharacterService,
              private messageService : MessageService) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.messageForm = this.formBuilder.group({
      messageContent: new FormControl('', Validators.required)
    });
  }

  onCloseDialog() {
    this.closeDialog.emit(false);
  }

  onCloseSucceed() {
    this.sendSucceed = false;
    this.initForm();
  }

  onCloseFailed() {
    this.sendFailed = false;
  }

  async confirmMessage(value: string) {
    this.display = false;
    this.onCloseDialog();

    this.messageService.saveMessage(value['messageContent'], this.ticketId).then(res => {
      this.sendSucceed = true;
    }).catch(e => {
      this.sendFailed = true;
    });
  }

}
