import { Component, OnInit } from '@angular/core';

import {ActivatedRoute, Router} from "@angular/router";
import {Exercise} from "../../../shared/models/exercise/exercise";
import {DamageQuestion} from "../../../shared/models/damage-question";
import {SelectedAnswer} from "../../../shared/models/selected-answer";
import {CharacterService} from "../../../core/services/character/character.service";
import {ExpeditionService} from "../../../core/services/expedition/expedition.service";
import {MessageService} from "primeng/api";
import {Character} from "../../../shared/models/character/character";
import {CharacterInfoDto} from "../../../shared/models/character-info-dto";
import {QuestionsService} from "../../../core/services/questions.service";
import {ExercicesService} from "../../../core/services/exercices.service";

@Component({
  selector: 'app-exercice',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.css'],
  providers: [MessageService]
})
export class ExerciseComponent implements OnInit {

  exercise:Exercise = null;

  monsterLife: number;
  lastDamage: DamageQuestion;
  exerciseMark: number = null;
  expeditionId: number = null;
  character: CharacterInfoDto;

  constructor(private questionsService: QuestionsService,
              private exerciseService: ExercicesService,
              private characterService: CharacterService,
              private expeditionService: ExpeditionService,
              private route: ActivatedRoute,
              private messageService: MessageService,
              private router: Router) {

  }


  ngOnInit(): void {
    const charId = localStorage.getItem("characterId");
    if (charId !== null) {
      this.characterService.getCharacterInformations(parseInt(charId)).then(result => {
        this.character = result;
      })
        .catch(e => {
          console.log(e);
        });
    }

    this.route.paramMap.subscribe(params => {
      if (params.get('exerciseId')) {
        this.questionsService.getAllQuestionByExercise(params.get('exerciseId')).then(result => {
          result.questions = result.questions.sort((q1,q2) => q1.sequence_number - q2.sequence_number);
          this.exercise = result;

          this.monsterLife = this.exercise.life;
        }).catch(error => {
          console.log(error)
        })
      }
      if (params.get('expeditionId')) {
        this.expeditionId = Number.parseInt(params.get('expeditionId'));
      }
    });
  }

  getDamageToSend(damage:DamageQuestion) {
    this.monsterLife -= damage.damage;
    this.lastDamage = damage;
  }

  setExerciseMark(answers: SelectedAnswer[]) {
    const charId = localStorage.getItem('characterId');
    if (charId == null) {
      //TODO
    }
    const correctAnswers = answers.filter(answer =>answer.answer != null && answer.answer.correct == true).length;
    this.exerciseMark = correctAnswers * 20 / answers.length;

    const characterAnswer = answers.map(answer => {
      answer.answer['questionId'] = answer.questionId;
      answer.answer['bonusConsumed'] = answer.bonusConsumed;
      return answer.answer
    });
    const data = {
      answers : characterAnswer,
      exerciseId: this.exercise.id,
      adventureId: this.expeditionId,
      characterId: parseInt(charId)
    };
    if (this.monsterLife <= 0) {
        this.characterService.addExperienceToCharacter(parseInt(charId), this.exercise.experience).then(() => {
        }).catch(console.error)
    }

    this.exerciseService.saveExerciseAnswer(data).then(result => {
        this.expeditionService.checkEndOfExpedition(this.expeditionId, parseInt(charId)).then(result => {
          if (result !== null) {
            this.messageService.add({severity:'success',id:"end-expedition", summary:'Fin \'expedition', detail:'Bravo Aventurier !\n En finissant l\'expédition tu as gagné ' + result.experience + ' points d\'experience.'  });
            setTimeout(() => {
              this.router.navigate(['/resume'])
            },10000)
          } else {
            setTimeout(() => {
              this.expeditionService.getExpeditionById(this.expeditionId).then(expedition => {
                 this.router.navigate(['/expedition',expedition.id, expedition.title])
              });
            },10000)
          }
        }).catch(error => {
          setTimeout(() => {
            this.expeditionService.getExpeditionById(this.expeditionId).then(expedition => {
              this.router.navigate(['/expedition',expedition.id, expedition.title])
            });
          },10000)
        })

    }).catch(error => {
      console.error(error)
    });

  }

  getExerciseMark() {
    return Math.round(this.exerciseMark);
  }
}
