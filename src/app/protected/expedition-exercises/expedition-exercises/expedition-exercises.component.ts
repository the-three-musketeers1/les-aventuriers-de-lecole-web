import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ExerciseWithCharacterData} from "../../../shared/models/exercise/exercise-with-character-data";
import {CharacterService} from "../../../core/services/character/character.service";
import {CharacterInfoDto} from "../../../shared/models/character-info-dto";
import {ExpeditionService} from "../../../core/services/expedition/expedition.service";
import {ExercicesService} from "../../../core/services/exercices.service";

@Component({
  selector: 'app-expedition-exercises',
  templateUrl: './expedition-exercises.component.html',
  styleUrls: ['./expedition-exercises.component.css']
})
export class ExpeditionExercisesComponent implements OnInit {

  exercices: ExerciseWithCharacterData[] = [];

  expedition: string;
  expeditionId: number;
    character: CharacterInfoDto;


  constructor(private exercicesService: ExercicesService,
              private route: ActivatedRoute,
              private characterService: CharacterService,
              private expeditionService: ExpeditionService) { }

  ngOnInit(): void {
    const charId = localStorage.getItem("characterId");
    if (charId !== null) {
      this.characterService.getCharacterInformations(parseInt(charId)).then(result => {
        this.character = result;
      })
        .catch(console.error);
    }

    this.route.paramMap.subscribe(params => {
      let characterId = localStorage.getItem('characterId');
      if (params.get('expeditionId') && params.get('expeditionName')){
        this.expeditionId = Number.parseInt(params.get('expeditionId'));
        this.expedition = params.get("expeditionName");
        this.exercicesService.getExercicesOfExpeditionsWithCharacterData(params.get('expeditionId'),Number.parseInt(characterId)).then(result => {
          this.exercices = result;
        }).catch(e => {
          console.error(e)
        });
      }
    });
  }

}
