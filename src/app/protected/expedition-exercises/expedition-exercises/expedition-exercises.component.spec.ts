import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpeditionExercisesComponent } from './expedition-exercises.component';

describe('ExpeditionExercisesComponent', () => {
  let component: ExpeditionExercisesComponent;
  let fixture: ComponentFixture<ExpeditionExercisesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpeditionExercisesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpeditionExercisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
