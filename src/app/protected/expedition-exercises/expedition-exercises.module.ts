import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpeditionExercisesComponent } from './expedition-exercises/expedition-exercises.component';
import {SharedModule} from '../../shared/shared.module';



@NgModule({
  declarations: [ExpeditionExercisesComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class ExpeditionExercisesModule {
}
