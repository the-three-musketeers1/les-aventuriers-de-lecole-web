import { Component, OnInit } from '@angular/core';
import {CharacterService} from "../../core/services/character/character.service";
import {CharacterInfoDto} from "../../shared/models/character-info-dto";

@Component({
  selector: 'app-expeditions',
  templateUrl: './expeditions.component.html',
  styleUrls: ['./expeditions.component.scss']
})
export class ExpeditionsComponent implements OnInit {
  character: CharacterInfoDto;

  constructor(private characterService: CharacterService) { }

  ngOnInit(): void {
    const charId = localStorage.getItem("characterId");
    if (charId !== null) {
      this.characterService.getCharacterInformations(parseInt(charId)).then(result => {
        this.character = result;
      })
        .catch(console.error);
    }
  }

}
