import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ResumeComponent} from './connected-home/resume/resume.component';
import {ExpeditionExercisesComponent} from './expedition-exercises/expedition-exercises/expedition-exercises.component';
import {ExerciseComponent} from './exercises/exercise/exercise.component';
import {AuthGuard} from '../core/guard/auth.guard';
import {StatisticsComponent} from './statistics/statistics/statistics.component';
import {ListCharactersComponent} from './list-characters/list-characters/list-characters.component';
import {CheckingGuildComponent} from './checking-guild/checking-guild/checking-guild.component';
import {FormCharacterComponent} from './form-character/form-character/form-character.component';
import {ExpeditionsComponent} from './expeditions/expeditions.component';
import {TicketsComponent} from './tickets/tickets.component';
import {MessagesComponent} from './messages/messages.component';
import {GuildResolver} from '../core/resolvers/guild.resolver';
import {TicketMessagesResolver} from '../core/resolvers/ticket.messages.resolver';
import {ExerciseExpeditionResolver} from '../core/resolvers/exercise.expedition.resolver';
import {CharacterGuard} from '../core/guard/character.guard';
import {CharacterStorageResolver} from '../core/resolvers/character-storage.resolver';

const routes: Routes = [
  {path: '', component: ResumeComponent, canActivate: [AuthGuard, CharacterGuard], resolve: [CharacterStorageResolver]},
  {path: 'characters', component: ListCharactersComponent, canActivate: [AuthGuard]},
  {path: 'check-guild', component: CheckingGuildComponent, canActivate: [AuthGuard]},
  {path: 'form-character/:guildId/token/:guildToken', component: FormCharacterComponent, canActivate: [AuthGuard], resolve: [GuildResolver]},
  {path: 'resume', component: ResumeComponent, canActivate: [AuthGuard, CharacterGuard]},
  {path: 'expedition/:expeditionId/:expeditionName', component: ExpeditionExercisesComponent, canActivate: [AuthGuard, CharacterGuard], resolve: [ExerciseExpeditionResolver]},
  {path: 'exercise/:expeditionId/:exerciseId', component: ExerciseComponent, canActivate: [AuthGuard, CharacterGuard]},
  {path: 'statistics', component: StatisticsComponent, canActivate: [AuthGuard, CharacterGuard]},
  {path: 'expeditions', component: ExpeditionsComponent, canActivate: [AuthGuard, CharacterGuard]},
  {path: 'tickets', component: TicketsComponent, canActivate: [AuthGuard, CharacterGuard]},
  {path: 'messages/:ticketId', component: MessagesComponent, canActivate: [AuthGuard, CharacterGuard], resolve: [TicketMessagesResolver]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtectedRoutingModule {
}
