import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {CharacterService} from '../../../core/services/character/character.service';
import {Character} from '../../../shared/models/character/character';
import {RoleService} from '../../../core/services/role/role.service';
import {RolePictureService} from "../../../core/services/role-picture.service";

@Component({
  selector: 'app-list-guilds',
  templateUrl: './list-characters.component.html',
  styleUrls: ['./list-characters.component.scss']
})
export class ListCharactersComponent implements OnInit {

  characters: Character[] = [];
  cols: any[];
  selectedCharacter: Character;
  translateRoles = {
    warrior: 'guerrier',
    priest: 'soigneur',
    wizard: 'sorcier',
    thief: 'voleur'
  };
  loading: any;
  roles: any[] = [];

  constructor(
    private router: Router,
    private characterService: CharacterService,
    private roleService: RoleService,
    private rolePictureService: RolePictureService
  ) {
  }

  ngOnInit(): void {
    if (this.characterService.getIdInLocalStorage()) {
      this.characterService.removeIdInLocalStorage();
    }
    this.cols = [
      { field: 'name', header: 'Nom' },
      { field: 'role', header: 'Role'},
      { field: 'level', header: 'Niveau'},
      { field: 'experience', header: 'Expérience'},
      { field: 'action_points', header: 'Points d\'actions'}
    ];
    this.characterService.getCharacters().then(characters => {
      this.characters = characters;
    });
    this.roleService.getRoles().then(roles => {
      roles.forEach(role => this.roles.push({label: role.name, value: role.name}));
    });
  }

  async checkGuild() {
    await this.router.navigate(['check-guild']);
  }

  rolePicturePath(roleName: string) {
    return this.rolePictureService.getRolePicturePathByRoleName(roleName);
  }

  async onRowSelect($event: any) {
    const data = $event.data;
    this.characterService.setIdInLocalStorage(data.id);
    await this.router.navigate(['resume']);
  }
}
