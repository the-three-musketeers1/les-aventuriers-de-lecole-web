import { Component, OnInit } from '@angular/core';
import {CharacterService} from "../../../core/services/character/character.service";
import {CharacterInfoDto} from "../../../shared/models/character-info-dto";

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {
  character: CharacterInfoDto;

  constructor(private characterService: CharacterService) { }

  ngOnInit(): void {
    const charId = localStorage.getItem("characterId");
    if (charId !== null) {
      this.characterService.getCharacterInformations(parseInt(charId)).then(result => {
        this.character = result;
      })
        .catch(console.error);
    }
  }

}
