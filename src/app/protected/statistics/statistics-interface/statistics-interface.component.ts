import {Component, OnInit, ViewChild} from '@angular/core';
import {DonutChartFactoryService} from "../../../core/factories/donut-chart-factory.service";
import {DateUtilsService} from "../../../shared/utils/date-utils.service";
import {CharacterExerciseService} from "../../../core/services/character-exercise.service";
import {SubjectService} from "../../../core/services/subject.service";

@Component({
  selector: 'app-statistics-interface',
  templateUrl: './statistics-interface.component.html',
  styleUrls: ['./statistics-interface.component.scss']
})
export class StatisticsInterfaceComponent implements OnInit {

  @ViewChild("exerciseChart") exerciseChart : any;

  @ViewChild("monsterChart") monsterChart : any;

  exerciseData: any;
  exerciseOptions: any;
  monsterOptions: any;
  monsterData: any;
  disciplines: any = [];
  selectedDiscipline: any = null;
  chapters: any = [];
  selectedChapter: any = null;
  beginDate: Date;
  endDate: Date;
  dateEnable: boolean = false;
  fr: any;
  charId: number;
  statsLength: number;

  constructor(private characterExerciseService: CharacterExerciseService,
              private donutsChartFactory: DonutChartFactoryService,
              private subjectService: SubjectService,
              private dateUtils: DateUtilsService) {

    this.fr = {
      firstDayOfWeek: 1,
      dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
      dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
      dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
      monthNames: ['Janvier ', 'Février ', 'Mars ', 'Avril ', 'Mai ', 'Juin ', 'Juillet ', 'Août ', 'Septembre ', 'Octobre ', 'Novembre ', 'Décembre '],
      monthNamesShort: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sep', 'Oct', 'Nov', 'Déc'],
      today: 'Aujourd\'hui',
      clear: 'Clear'
    };
    this.charId = parseInt(localStorage.getItem("characterId"));
    this.endDate = new Date();
    this.beginDate = new Date();
    this.beginDate.setDate(this.beginDate.getDate() - 7);

    this.subjectService.getDisciplineByCharacterId(this.charId).then(disciplines => {
      this.disciplines = disciplines;
    }).catch(console.error);

    this.characterExerciseService.getStatisticsExercises(this.charId,null,null,null,null).then(stats => {
      this.statsLength = stats.length;
      this.exerciseData = this.donutsChartFactory.getAnswersDataFromStatisticsExercise(stats);
      this.monsterData = this.donutsChartFactory.getMonstersDataFromStatisticsExercise(stats);
    }).catch(err =>console.error(err));

    this.exerciseOptions = {
      legend: {
        position: 'right',
        fullWidth: false
      },
      title: {
        display: true,
        text: 'Réponses aux exercices',
        fontSize: 16
      },
    };
    this.monsterOptions = {
      legend: {
        position: 'right',
        fullWidth: false
      },
      title: {
        display: true,
        text: 'Monstres Rencontrés',
        fontSize: 16
      },
    };
  }

  ngOnInit(): void {
  }

  onDisciplineChange() {
    if (this.selectedDiscipline === null) {
      this.updateStats(this.charId,null,null);
    } else {
      this.subjectService.getChapterByDisciplineId(this.selectedDiscipline.id).then(chapters => {
        this.chapters = chapters;
      }).then(() => {
        this.updateStats(this.charId,this.selectedDiscipline.id,null)
      }).catch(console.error);
    }
  }

  onChapterChange() {
    if (this.selectedChapter === null) {
      this.updateStats(this.charId,this.selectedDiscipline.id,null);
    } else {
      this.updateStats(this.charId,this.selectedDiscipline.id,this.selectedChapter.id);
    }
  }

  updateStats(characterId, disciplineId, chapterId) {
    let beginDate = null;
    let endDate = null;
    if (this.dateEnable) {
      beginDate = this.dateUtils.formatDateToString(this.beginDate);
      endDate = this.dateUtils.formatDateToString(this.endDate);
    }
    this.characterExerciseService.getStatisticsExercises(characterId, disciplineId, chapterId, beginDate, endDate).then(stats => {
      this.statsLength = stats.length;
      this.exerciseData = this.donutsChartFactory.getAnswersDataFromStatisticsExercise(stats);
      this.monsterData = this.donutsChartFactory.getMonstersDataFromStatisticsExercise(stats);
      this.exerciseChart.refresh();
      this.monsterChart.refresh();
    }).catch(err =>console.error(err));
  }

  displayChapterDropdown() {
    return this.selectedDiscipline !== null && this.chapters !== null
  }

  changeDateStatut() {
    this.dateEnable = !this.dateEnable;
    this.updateStats(this.charId, this.selectedDiscipline ? this.selectedDiscipline.id : null, this.selectedChapter ? this.selectedChapter.id : null);

  }

  getLabelState() {
    if (this.dateEnable == true) {
      return "Intervalle"
    } else {
      return "Libre"
    }
  }

  onChangeDate() {
    this.updateStats(this.charId, this.selectedDiscipline ? this.selectedDiscipline.id : null, this.selectedChapter ? this.selectedChapter.id : null);
  }
}
