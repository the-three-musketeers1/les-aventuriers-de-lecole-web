import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsInterfaceComponent } from './statistics-interface.component';

describe('StatisticsInterfaceComponent', () => {
  let component: StatisticsInterfaceComponent;
  let fixture: ComponentFixture<StatisticsInterfaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticsInterfaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsInterfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
