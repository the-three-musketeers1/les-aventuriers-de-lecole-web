import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticsComponent } from './statistics/statistics.component';
import {SharedModule} from "../../shared/shared.module";
import { StatisticsInterfaceComponent } from './statistics-interface/statistics-interface.component';
import {ChartModule} from "primeng/chart";
import {DropdownModule} from "primeng/dropdown";
import {CalendarModule} from "primeng/calendar";
import {TabViewModule} from "primeng/tabview";



@NgModule({
  declarations: [StatisticsComponent, StatisticsInterfaceComponent],
  imports: [
    CommonModule,
    SharedModule,
    ChartModule,
    DropdownModule,
    CalendarModule,
    TabViewModule
  ]
})
export class StatisticsModule { }
